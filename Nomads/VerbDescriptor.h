#ifndef VERB_DESCRIPTOR_H
#define VERB_DESCRIPTOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DescriptorCommon.h"
#include "Inventory.h"
#include "Tagon.h"

typedef struct VerbDescriptor VERBDESCRIPTOR;

VERBTYPE VerbDescriptor_getType(VERBDESCRIPTOR* ptr);
PATTERNTYPE VerbDescriptor_getPattern(VERBDESCRIPTOR* ptr);
COLORTYPE VerbDescriptor_getForeground(VERBDESCRIPTOR* ptr);
COLORTYPE VerbDescriptor_getBackground(VERBDESCRIPTOR* ptr);

ITEMTYPE VerbDescriptor_getItem(VERBDESCRIPTOR* ptr);
const char* VerbDescriptor_getCaption(VERBDESCRIPTOR* ptr);
int VerbDescriptor_canDoVerb(VERBDESCRIPTOR* ptr, INVENTORY* inventory);
int VerbDescriptor_getEnergy(VERBDESCRIPTOR* ptr);

void VerbDescriptor_use(VERBDESCRIPTOR* ptr, TAGON* tagon);

#ifdef __cplusplus
}
#endif

#endif

