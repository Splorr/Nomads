#ifndef CRAFT_SCREEN_INTERNALS_H
#define CRAFT_SCREEN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "CraftScreen.h"
#include "ViewCommon_internals.h"
#include "RecipeType.h"
#include "ItemType.h"
#include "Scroller_internals.h"

	struct CraftScreen {
		VIEWCOMMON viewCommon;
		ITEMTYPE inventoryItems[ITEM_COUNT];
		RECIPETYPE recipes[RECIPE_COUNT];
		SCROLLER scrollers[CRAFTSCREENSCROLLER_COUNT];
		CRAFTSCREENSCROLLERTYPE currentScroller;
	};

#ifdef __cplusplus
}
#endif

#endif

