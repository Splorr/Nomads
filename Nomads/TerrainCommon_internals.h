#ifndef TERRAIN_COMMON_INTERNALS_H
#define TERRAIN_COMMON_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TerrainCommon.h"

	struct TerrainCommon {
		TERRAINTYPE type;
	};

#ifdef __cplusplus
}
#endif

#endif

