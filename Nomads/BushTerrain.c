#include "BushTerrain_internals.h"
#include <assert.h>
#include "ItemType.h"
#include "Generators.h"
#include "TerrainDescriptor.h"
#include "MapCell.h"
#include "TerrainDescriptors.h"
#include "Terrain_internals.h"

TERRAINTYPE BushTerrain_getType(BUSHTERRAIN* ptr)
{
	assert(ptr);
	return TerrainCommon_getType(BushTerrain_getCommon(ptr));
}

TERRAINCOMMON* BushTerrain_getCommon(BUSHTERRAIN* ptr)
{
	assert(ptr);
	return &(ptr->common);
}

BERRYTYPE BushTerrain_getBerry(BUSHTERRAIN* ptr)
{
	assert(ptr);
	return ptr->berry;
}

int BushTerrain_getBerryTimer(BUSHTERRAIN* ptr)
{
	assert(ptr);
	return ptr->berryTimer;
}

int BushTerrain_getDeathTimer(BUSHTERRAIN* ptr)
{
	assert(ptr);
	return ptr->deathTimer;
}

void BushTerrain_setBerry(BUSHTERRAIN* ptr, BERRYTYPE berry)
{
	assert(ptr);
	assert(berry >= BERRY_FIRST && berry < BERRY_COUNT);
	ptr->berry = berry;
}

void BushTerrain_setBerryTimer(BUSHTERRAIN* ptr, int value)
{
	assert(ptr);
	ptr->berryTimer = value;
}

void BushTerrain_setDeathTimer(BUSHTERRAIN* ptr, int value)
{
	assert(ptr);
	ptr->deathTimer = value;
}

void BushTerrain_changeBerryTimerBy(BUSHTERRAIN* ptr, int delta)
{
	assert(ptr);
	BushTerrain_setBerryTimer(ptr, BushTerrain_getBerryTimer(ptr) + delta);
}

void BushTerrain_changeDeathTimerBy(BUSHTERRAIN* ptr, int delta)
{
	assert(ptr);
	BushTerrain_setDeathTimer(ptr, BushTerrain_getDeathTimer(ptr) + delta);
}

void BushTerrain_doTimer(union TerrainDescriptor* ptr, struct MapCell* cell)
{
	assert(ptr);
	assert(cell);
	BUSHTERRAIN* bush = Terrain_getBush(MapCell_getTerrain(cell));

	BushTerrain_changeBerryTimerBy(bush, -1);
	if (BushTerrain_getBerryTimer(bush) <= 0)
	{
		BushTerrain_setBerryTimer(bush, Generator_generate(Generators_getGenerator(GENERATOR_BERRYTIMER)));
		BERRYTYPE berry = BushTerrain_getBerry(bush);
		ITEMTYPE berryItem =
			(berry == BERRY_AMETHYST) ? (ITEM_AMETHYST_BERRY) :
			(berry == BERRY_CARNELIAN) ? (ITEM_CARNELIAN_BERRY) :
			(berry == BERRY_COPPER) ? (ITEM_COPPER_BERRY) :
			(berry == BERRY_GOLD) ? (ITEM_GOLD_BERRY) :
			(berry == BERRY_JADE) ? (ITEM_JADE_BERRY) :
			(berry == BERRY_RUBY) ? (ITEM_RUBY_BERRY) :
			(berry == BERRY_SILVER) ? (ITEM_SILVER_BERRY) :
			(berry == BERRY_TURQUOISE) ? (ITEM_TURQUOISE_BERRY) : (ITEM_NONE);
		if (berryItem != ITEM_NONE)
		{
			MapCell_changeInventoryCountBy(cell, berryItem, 1);
		}

	}

	BushTerrain_changeDeathTimerBy(bush, -1);
	if (BushTerrain_getDeathTimer(bush) <= 0)
	{
		TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_DEAD_BUSH), MapCell_getTerrain(cell));
	}
}

void DeadBushTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)//todo: move me to deadbush.c
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_DEAD_BUSH;
}

void BushTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_BUSH;
	ptr->bush.berryTimer = Generator_generate(Generators_getGenerator(GENERATOR_BERRYTIMER));
	ptr->bush.deathTimer = Generator_generate(Generators_getGenerator(GENERATOR_BERRYDEATHTIMER));
	ptr->bush.berry = Generator_generate(Generators_getGenerator(GENERATOR_BERRYTYPE));
}

