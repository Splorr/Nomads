#ifndef TERRAIN_TYPE_H
#define TERRAIN_TYPE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum TerrainType{
	TERRAIN_GRASS,
	TERRAIN_TREE,
	TERRAIN_ROCK,
	TERRAIN_FLINT_ROCK,
	TERRAIN_BUSH,
	TERRAIN_RABBIT_HOLE,
	TERRAIN_WATER,
	TERRAIN_STUMP,
	TERRAIN_RUBBLE,
	TERRAIN_DEAD_BUSH,
	TERRAIN_BERRY_STARTER,
	TERRAIN_DIRT,
	TERRAIN_ACORN,

	TERRAIN_COUNT,
	TERRAIN_FIRST = TERRAIN_GRASS
} TERRAINTYPE;

#ifdef __cplusplus
}
#endif

#endif

