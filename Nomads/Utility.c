#include "Utility.h"
#include <assert.h>

int Utility_modulo(int value, int max)
{
	assert(max > 0);
	value %= max;
	return (value < 0) ? (value + max) : (value);
}

int Utility_minimum(int first, int second)
{
	return (first < second) ? (first) : (second);
}

int Utility_maximum(int first, int second)
{
	return (first > second) ? (first) : (second);
}

