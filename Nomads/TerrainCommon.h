#ifndef TERRAIN_COMMON_H
#define TERRAIN_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TerrainType.h"

typedef struct TerrainCommon TERRAINCOMMON;

TERRAINTYPE TerrainCommon_getType(TERRAINCOMMON* ptr);
void TerrainCommon_setType(TERRAINCOMMON* ptr, TERRAINTYPE terrain);

#ifdef __cplusplus
}
#endif

#endif

