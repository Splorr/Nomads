#ifndef DESCRIPTOR_COMMON_INTERNALS_H
#define DESCRIPTOR_COMMON_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DescriptorCommon.h"

	struct DescriptorCommon {
		union {
			TERRAINTYPE terrainType;
			ITEMTYPE itemType;
			CREATURETYPE creatureType;
			VERBTYPE verbType;
		} type;
		PATTERNTYPE pattern;
		COLORTYPE foreground;
		COLORTYPE background;
	};

#ifdef __cplusplus
}
#endif

#endif
