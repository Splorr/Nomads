#ifndef ACORN_TERRAIN_INTERNALS_H
#define ACORN_TERRAIN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "AcornTerrain.h"
#include "TerrainCommon_internals.h"

	struct AcornTerrain {
		TERRAINCOMMON common;
		int treeTimer;
	};

	union TerrainDescriptor;
	union Terrain;
	struct MapCell;

	void AcornTerrain_doTimer(union TerrainDescriptor* ptr, struct MapCell* cell);
	void AcornTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);

#ifdef __cplusplus
}
#endif

#endif

