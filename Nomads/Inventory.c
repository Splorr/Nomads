#include "Inventory_internals.h"
#include "Utility.h"
#include <assert.h>

void Inventory_clear(INVENTORY* ptr)
{
	assert(ptr);
	for (ITEMTYPE item = ITEM_FIRST; item < ITEM_COUNT; ++item)
	{
		Inventory_setCount(ptr, item, 0);
	}
}

void Inventory_setCount(INVENTORY* ptr, ITEMTYPE item, int count)
{
	assert(ptr);
	if (item >= ITEM_FIRST && item < ITEM_COUNT)
	{
		ptr->counts[item] = Utility_maximum(0, count);
	}
}

int Inventory_getCount(INVENTORY* ptr, ITEMTYPE item)
{
	assert(ptr);
	return (item >= ITEM_FIRST && item < ITEM_COUNT) ? (ptr->counts[item]) : (0);
}

int Inventory_hasCount(INVENTORY* ptr, ITEMTYPE item)
{
	assert(ptr);
	return Inventory_getCount(ptr, item) > 0;
}

void Inventory_add(INVENTORY* ptr, INVENTORY* first, INVENTORY* second)
{
	assert(ptr);
	assert(first);
	assert(second);
	for (ITEMTYPE item = ITEM_FIRST; item < ITEM_COUNT; ++item)
	{
		Inventory_setCount(ptr, item, Inventory_getCount(first, item) + Inventory_getCount(second, item));
	}
}

void Inventory_subtract(INVENTORY* ptr, INVENTORY* first, INVENTORY* second)
{
	assert(ptr);
	assert(first);
	assert(second);
	for (ITEMTYPE item = ITEM_FIRST; item < ITEM_COUNT; ++item)
	{
		Inventory_setCount(ptr, item, Inventory_getCount(first, item) - Inventory_getCount(second, item));
	}
}

void Inventory_changeCountBy(INVENTORY* ptr, ITEMTYPE item, int delta)
{
	assert(ptr);
	Inventory_setCount(ptr, item, Inventory_getCount(ptr, item) + delta);
}

ITEMTYPE Inventory_getFirstItem(INVENTORY* ptr)
{
	assert(ptr);
	for (ITEMTYPE item = ITEM_FIRST; item < ITEM_COUNT; ++item)
	{
		if (Inventory_hasCount(ptr, item) > 0)
		{
			return item;
		}
	}
	return ITEM_NONE;
}

int Inventory_contains(INVENTORY* ptr, INVENTORY* subset)
{
	assert(ptr);
	assert(subset);
	for (ITEMTYPE item = ITEM_FIRST; item < ITEM_COUNT; ++item)
	{
		if (Inventory_getCount(subset, item) > Inventory_getCount(ptr, item))
		{
			return 0;
		}
	}
	return 1;
}

int Inventory_getTotalCount(INVENTORY* ptr)
{
	assert(ptr);
	int total = 0;
	for (ITEMTYPE item = ITEM_FIRST; item < ITEM_COUNT; ++item)
	{
		total += Inventory_getCount(ptr, item);
	}
	return total;
}


