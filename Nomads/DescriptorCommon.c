#include "DescriptorCommon_internals.h"
#include <assert.h>

VERBTYPE DescriptorCommon_getVerbType(DESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return ptr->type.verbType;
}

TERRAINTYPE DescriptorCommon_getTerrainType(DESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return ptr->type.terrainType;
}

CREATURETYPE DescriptorCommon_getCreatureType(DESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return ptr->type.creatureType;
}

ITEMTYPE DescriptorCommon_getItemType(DESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return ptr->type.itemType;
}

PATTERNTYPE DescriptorCommon_getPattern(DESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return ptr->pattern;
}

COLORTYPE DescriptorCommon_getForeground(DESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return ptr->foreground;
}

COLORTYPE DescriptorCommon_getBackground(DESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return ptr->background;
}
