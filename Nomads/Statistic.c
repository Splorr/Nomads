#include "Statistic_internals.h"
#include <assert.h>

int Statistic_getCurrent(STATISTIC* ptr)
{
	assert(ptr);
	return ptr->current;
}

int Statistic_getMinimum(STATISTIC* ptr)
{
	assert(ptr);
	return ptr->minimum;
}

int Statistic_getMaximum(STATISTIC* ptr)
{
	assert(ptr);
	return ptr->maximum;
}

void Statistic_initialize(STATISTIC* ptr, int current, int minimum, int maximum)
{
	assert(ptr);
	assert(maximum >= minimum);
	ptr->minimum = minimum;
	ptr->maximum = maximum;
	Statistic_setCurrent(ptr,current);
}

void Statistic_setCurrent(STATISTIC* ptr, int current)
{
	assert(ptr);
	ptr->current = (current < ptr->minimum) ? (ptr->minimum) : (current > ptr->maximum) ? (ptr->maximum) : (current);
}

void Statistic_setMinimum(STATISTIC* ptr, int minimum)
{
	assert(ptr);
	ptr->minimum = minimum;
	Statistic_setCurrent(ptr, Statistic_getCurrent(ptr));
}

void Statistic_setMaximum(STATISTIC* ptr, int maximum)
{
	assert(ptr);
	ptr->maximum = maximum;
	Statistic_setCurrent(ptr, Statistic_getCurrent(ptr));
}

int Statistic_hasAtLeast(STATISTIC* ptr, int value)
{
	assert(ptr);
	return Statistic_getCurrent(ptr)>=value;
}

void Statistic_changeBy(STATISTIC* ptr, int delta)
{
	assert(ptr);
	Statistic_setCurrent(ptr,Statistic_getCurrent(ptr)+delta);
}
