#ifndef TEXT_PLOTTER_H
#define TEXT_PLOTTER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ColorType.h"

void TextPlotter_plotText(int x, int y, const char* text, COLORTYPE foreground, COLORTYPE background);
void TextPlotter_centerText(int x, int y, const char* text, COLORTYPE foreground, COLORTYPE background);

#ifdef __cplusplus
}
#endif

#endif

