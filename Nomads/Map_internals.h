#ifndef MAP_INTERNALS_H
#define MAP_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Constants.h"
#include "MapColumn.h"
#include "MapColumn_internals.h"
#include "Map.h"

struct Map {
	MAPCOLUMN columns[MAP_COLUMNS];
};

void Map_findTerrain(MAP* ptr, TERRAIN* terrain, int* x, int *y);

#ifdef __cplusplus
}
#endif

#endif

