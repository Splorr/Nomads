#ifndef GRAVEL_DESCRIPTOR_INTERNALS_H
#define GRAVEL_DESCRIPTOR_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ItemDescriptorCommon_internals.h"

	void GravelDescriptor_use(union ItemDescriptor* descriptor, struct Tagon* tagon, VERBTYPE verb);

#ifdef __cplusplus
}
#endif

#endif

