#ifndef TERRAIN_H
#define TERRAIN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TerrainType.h"
#include "TerrainDescriptor.h"
#include "TreeTerrain.h"
#include "BushTerrain.h"
#include "RockTerrain.h"
#include "FlintrockTerrain.h"
#include "BerryStarterTerrain.h"
#include "DirtTerrain.h"
#include "AcornTerrain.h"

typedef union Terrain TERRAIN;

struct MapCell;

void Terrain_initialize(TERRAIN* ptr, TERRAINDESCRIPTOR* descriptor);
void Terrain_doTimers(struct MapCell* ptr);//TODO: switch this to terrain instead of mapcell
TERRAINTYPE Terrain_getType(TERRAIN* ptr);
TREETERRAIN* Terrain_getTree(TERRAIN* ptr);
BUSHTERRAIN* Terrain_getBush(TERRAIN* ptr);
ROCKTERRAIN* Terrain_getRock(TERRAIN* ptr);
FLINTROCKTERRAIN* Terrain_getFlintrock(TERRAIN* ptr);
BERRYSTARTERTERRAIN* Terrain_getBerryStarter(TERRAIN* ptr);
DIRTTERRAIN* Terrain_getDirt(TERRAIN* ptr);
ACORNTERRAIN* Terrain_getAcorn(TERRAIN* ptr);

#ifdef __cplusplus
}
#endif

#endif

