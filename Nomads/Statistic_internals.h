#ifndef STATISTIC_INTERNALS_H
#define STATISTIC_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Statistic.h"

	struct Statistic {
		int current;
		int minimum;
		int maximum;
	};

	void Statistic_initialize(STATISTIC* ptr, int current, int minimum, int maximum);

#ifdef __cplusplus
}
#endif

#endif

