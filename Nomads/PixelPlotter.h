#ifndef PIXEL_PLOTTER_H
#define PIXEL_PLOTTER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ColorType.h"

typedef struct PixelPlotterState PIXELPLOTTERSTATE;

void PixelPlotterState_initialize(PIXELPLOTTERSTATE* state,int offsetX, int offsetY,int scaleX,int scaleY,int clipEnabled,int clipWidth,int clipHeight);

void PixelPlotter_setRenderer(void* renderer);
void* PixelPlotter_getRenderer(void);

void PixelPlotter_setState(PIXELPLOTTERSTATE* state);
void PixelPlotter_getState(PIXELPLOTTERSTATE* state);

void PixelPlotter_setOffsetX(int offsetX);
void PixelPlotter_setOffsetY(int offsetY);
void PixelPlotter_setScaleX(int scaleX);
void PixelPlotter_setScaleY(int scaleY);
void PixelPlotter_setClipEnabled(int clipEnabled);
void PixelPlotter_setClipWidth(int clipWidth);
void PixelPlotter_setClipHeight(int clipHeight);

int PixelPlotter_getOffsetX(void);
int PixelPlotter_getOffsetY(void);
int PixelPlotter_getScaleX(void);
int PixelPlotter_getScaleY(void);
int PixelPlotter_getClipEnabled(void);
int PixelPlotter_getClipWidth(void);
int PixelPlotter_getClipHeight(void);

void PixelPlotter_plotPixel(int x, int y, COLORTYPE colorType);
void PixelPlotter_plotRect(int x, int y, int w, int h, COLORTYPE colorType);

void PixelPlotter_setScale(int x, int y);
void PixelPlotter_setOffset(int x, int y);


#ifdef __cplusplus
}
#endif

#endif

