#ifndef VIEWS_H
#define VIEWS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewType.h"
#include "View.h"
#include "ViewCommandType.h"

void Views_initialize(void);

VIEW* Views_getView(VIEWTYPE viewType);
void Views_draw(void);
void Views_process(VIEWCOMMANDTYPE command);

void Views_enable(VIEWTYPE viewType);
void Views_disable(VIEWTYPE viewType);
int Views_isEnabled(VIEWTYPE viewType);
void Views_enableAll(void);
void Views_disableAll(void);
int Views_countEnabled(void);

#ifdef __cplusplus
}
#endif

#endif

