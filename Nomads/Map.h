#ifndef MAP_H
#define MAP_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Constants.h"
#include "MapColumn.h"

typedef struct Map MAP;

MAPCOLUMN* Map_getMapColumn(MAP* ptr, int column);
void Map_doTimers(MAP* ptr);

MAPCELL* Map_getMapCell(MAP* ptr, int column, int row);

#ifdef __cplusplus
}
#endif

#endif

