#ifndef VIEW_COMMON_INTERNALS_H
#define VIEW_COMMON_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewCommon.h"
#include "PixelPlotter_internals.h"

	struct ViewCommon {
		VIEWTYPE viewType;
		int enabled;
		PIXELPLOTTERSTATE pixelPlotterState;
	};

#ifdef __cplusplus
}
#endif

#endif

