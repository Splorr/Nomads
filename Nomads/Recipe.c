#include "Recipe_internals.h"
#include <assert.h>

INVENTORY* Recipe_getInput(RECIPE* ptr)
{
	assert(ptr);
	return &(ptr->input);
}

INVENTORY* Recipe_getOutput(RECIPE* ptr)
{
	assert(ptr);
	return &(ptr->output);
}

int Recipe_canCraft(RECIPE* ptr, INVENTORY* inventory)
{
	assert(ptr);
	assert(inventory);
	return Inventory_contains(inventory, Recipe_getInput(ptr));
}

void Recipe_craft(RECIPE* ptr, INVENTORY* inventory)
{
	assert(ptr);
	assert(inventory);
	Inventory_subtract(inventory, inventory, Recipe_getInput(ptr));
	Inventory_add(inventory, inventory, Recipe_getOutput(ptr));
}
