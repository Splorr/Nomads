#include "View_internals.h"
#include "TextPlotter.h"
#include "PixelPlotter_internals.h"
#include "Views.h"
#include "Constants.h"
#include "MainMenu_internals.h"
#include <assert.h>

#define MAINMENU_WIDTH (PATTERN_WIDTH * 9)
#define MAINMENU_HEIGHT (PATTERN_HEIGHT * 5)

static const char* mainMenuItemTexts[MAINMENUITEM_COUNT] = {"  Play   ","  Load   ","  Save   ","  Quit   "};
static int mainMenuItemYs[MAINMENUITEM_COUNT] = { PATTERN_WIDTH * 1, PATTERN_WIDTH * 2, PATTERN_WIDTH * 3, PATTERN_WIDTH * 4, };

void MainMenu_initialize(union View* ptr)
{
	assert(ptr);
	PixelPlotterState_initialize(View_getPixelPlotterState(ptr),
	SCALE_CENTER_X(SCALE_LARGE,MAINMENU_WIDTH),
	SCALE_CENTER_Y(SCALE_LARGE,MAINMENU_HEIGHT),
	SCALE_LARGE,
	SCALE_LARGE,
	1,
	MAINMENU_WIDTH,
	MAINMENU_HEIGHT);

	ptr->mainMenu.viewCommon.viewType = VIEW_MAIN_MENU;
	ptr->mainMenu.viewCommon.enabled = 0;
	ptr->mainMenu.currentItem = MAINMENUITEM_CONTINUE;
}

void MainMenu_draw(union View* ptr)
{
	assert(ptr);
	PIXELPLOTTERSTATE old;
	PixelPlotter_getState(&old);
	PixelPlotter_setState(View_getPixelPlotterState(ptr));

	TextPlotter_plotText(0, 0, "Main Menu", COLOR_ONYX, COLOR_SILVER);

	COLORTYPE foreground;
	COLORTYPE background;
	for (int index = 0; index < MAINMENUITEM_COUNT; ++index)
	{
		foreground = (index == ptr->mainMenu.currentItem) ? (COLOR_AMETHYST) : (COLOR_DARK_AMETHYST);
		background = (index == ptr->mainMenu.currentItem) ? (COLOR_ONYX) : (COLOR_ONYX);
		TextPlotter_plotText(0, mainMenuItemYs[index], mainMenuItemTexts[index], foreground, background);
	}

	PixelPlotter_setState(&old);
}

int MainMenu_process(union View* ptr,VIEWCOMMANDTYPE command)
{
	assert(ptr);
	switch (command)
	{
	case VIEWCOMMAND_MOVE_UP:
		ptr->mainMenu.currentItem += (MAINMENUITEM_COUNT - 1);
		ptr->mainMenu.currentItem %= MAINMENUITEM_COUNT;
		return 1;
	case VIEWCOMMAND_MOVE_DOWN:
		ptr->mainMenu.currentItem ++;
		ptr->mainMenu.currentItem %= MAINMENUITEM_COUNT;
		return 1;
	case VIEWCOMMAND_BUTTON_A:
		switch (ptr->mainMenu.currentItem)
		{
		case MAINMENUITEM_CONTINUE:
			Views_disable(VIEW_MAIN_MENU);
			break;
		case MAINMENUITEM_QUIT:
			Views_disable(VIEW_MAIN_MENU);
			Views_enable(VIEW_CONFIRM_QUIT);
			break;
		}
		return 1;
	default:
		return 0;
	}
}

