#ifndef CONFIRM_QUIT_H
#define CONFIRM_QUIT_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewCommon.h"
#include "ViewCommandType.h"

typedef enum ConfirmQuitItemType{
	CONFIRMQUITITEM_NO,
	CONFIRMQUITITEM_YES,
	CONFIRMQUITITEM_COUNT,
	CONFIRMQUITITEM_FIRST = CONFIRMQUITITEM_NO
} CONFIRMQUITITEMTYPE;

typedef struct ConfirmQuit CONFIRMQUIT;

union View;

void ConfirmQuit_draw(union View*);
int ConfirmQuit_process(union View*, VIEWCOMMANDTYPE);
void ConfirmQuit_initialize(union View*);

#ifdef __cplusplus
}
#endif

#endif
