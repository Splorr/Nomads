#ifndef CREATURE_H
#define CREATURE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "CreatureDescriptor.h"
#include "Tagon.h"

typedef union Creature CREATURE;

struct MapCell;

void Creature_clear(CREATURE* ptr);
void Creature_initialize(CREATURE* ptr, CREATUREDESCRIPTOR* descriptor);
void Creature_swap(CREATURE* ptr, CREATURE* other);
void Creature_postInteract(CREATURE* ptr);
TAGON* Creature_getTagon(CREATURE* ptr);


void Creature_doTimers(CREATURE* ptr);

CREATURETYPE Creature_getType(CREATURE* ptr);

#ifdef __cplusplus
}
#endif

#endif
