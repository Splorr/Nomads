#include "Scroller_internals.h"
#include <assert.h>
#include "Utility.h"

void Scroller_initialize(SCROLLER* ptr, int itemCount, int listSize)
{
	assert(ptr);
	ptr->itemCount = itemCount;
	ptr->listSize = listSize;
	ptr->index = 0;
	ptr->scroll = 0;
}

int Scroller_getItemCount(SCROLLER* ptr)
{
	assert(ptr);
	return ptr->itemCount;
}

int Scroller_getListSize(SCROLLER* ptr)
{
	assert(ptr);
	return ptr->listSize;
}

int Scroller_getScroll(SCROLLER* ptr)
{
	assert(ptr);
	return ptr->scroll;
}

int Scroller_getIndex(SCROLLER* ptr)
{
	assert(ptr);
	return ptr->index;
}

int Scroller_getMaximumScroll(SCROLLER* ptr)
{
	assert(ptr);
	return (ptr->listSize>ptr->itemCount) ? (0) : (ptr->itemCount - ptr->listSize);
}

void Scroller_refresh(SCROLLER* ptr)
{
	assert(ptr);
	if (ptr->itemCount == 0) {
		ptr->index = 0;
	}
	else {
		ptr->index = Utility_modulo(ptr->index, ptr->itemCount);
	}
	if (ptr->index<ptr->scroll) {
		ptr->scroll = ptr->index;
	}
	else if (ptr->index >= ptr->scroll + ptr->listSize) {
		ptr->scroll = ptr->index - ptr->listSize + 1;
	}
	if (ptr->scroll>Scroller_getMaximumScroll(ptr)) {
		ptr->scroll = Scroller_getMaximumScroll(ptr);
	}
}

void Scroller_setItemCount(SCROLLER* ptr, int itemCount)
{
	assert(ptr);
	ptr->itemCount = itemCount;
	Scroller_refresh(ptr);
}

void Scroller_nextIndex(SCROLLER* ptr)
{
	assert(ptr);
	ptr->index = (ptr->index + 1) % ptr->itemCount;
}

void Scroller_previousIndex(SCROLLER* ptr)
{
	assert(ptr);
	ptr->index = (ptr->index + ptr->itemCount - 1) % ptr->itemCount;
}

int Scroller_getThumbTop(SCROLLER* ptr, int offset, int height)
{
	assert(ptr);
	if (Scroller_getMaximumScroll(ptr) == 0) {
		return offset;
	}
	else {
		return offset + height * ptr->scroll / ptr->itemCount;
	}
}

int Scroller_getThumbBottom(SCROLLER* ptr, int offset, int height)
{
	assert(ptr);
	if (Scroller_getMaximumScroll(ptr) == 0) {
		return offset + height;
	}
	else {
		return offset + height * (ptr->scroll + ptr->listSize) / ptr->itemCount;
	}
}

void Scroller_getItemIndices(SCROLLER* ptr, int indexCount, int* indices)
{
	assert(ptr);
	for (int offset = 0; offset < indexCount; ++offset)
	{
		int index = offset + Scroller_getScroll(ptr);
		if (index < Scroller_getItemCount(ptr))
		{
			indices[offset] = index;
		}
		else
		{
			indices[offset] = -1;
		}
	}
}

int Scroller_canScrollUp(SCROLLER* ptr)
{
	assert(ptr);
	return Scroller_getScroll(ptr) > 0;
}

int Scroller_canScrollDown(SCROLLER* ptr)
{
	assert(ptr);
	return (Scroller_getScroll(ptr) + Scroller_getListSize(ptr)) < Scroller_getItemCount(ptr);
}
