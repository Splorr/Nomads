#ifndef RECIPE_H
#define RECIPE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Inventory.h"

typedef struct Recipe RECIPE;

INVENTORY* Recipe_getInput(RECIPE* ptr);
INVENTORY* Recipe_getOutput(RECIPE* ptr);
int Recipe_canCraft(RECIPE* ptr, INVENTORY* inventory);
void Recipe_craft(RECIPE* ptr, INVENTORY* inventory);

#ifdef __cplusplus
}
#endif

#endif

