#include "MapCell_internals.h"
#include "TerrainDescriptors.h"
#include <assert.h>

int MapCell_canEnter(MAPCELL* ptr, CREATURE* creature)
{
	assert(ptr);
	assert(creature);
	if (Creature_getType(MapCell_getCreature(ptr)) != CREATURE_NONE)
	{
		return 0;
	}
	TERRAINTYPE terrainType = Terrain_getType(MapCell_getTerrain(ptr));
	TERRAINDESCRIPTOR* descriptor = TerrainDescriptors_getDescriptor(terrainType);
	return TerrainDescriptor_canEnter(descriptor, creature);
}

void MapCell_doTimers(MAPCELL* ptr)
{
	assert(ptr);
	Terrain_doTimers(ptr);
	Creature_doTimers(MapCell_getCreature(ptr));
}

int MapCell_canInteract(MAPCELL* ptr, CREATURE* creature, VERBTYPE verb)//TODO: refactor to get rid of this
{
	assert(ptr);
	assert(creature);
	TERRAINTYPE terrainType = Terrain_getType(MapCell_getTerrain(ptr));
	TERRAINDESCRIPTOR* descriptor = TerrainDescriptors_getDescriptor(terrainType);
	return TerrainDescriptor_canInteract(descriptor, creature, verb);
}

void MapCell_interact(MAPCELL* ptr, CREATURE* creature, VERBTYPE verb)
{
	assert(ptr);
	assert(creature);
	TERRAIN* terrain = MapCell_getTerrain(ptr);
	TERRAINTYPE terrainType = Terrain_getType(terrain);
	TERRAINDESCRIPTOR* descriptor = TerrainDescriptors_getDescriptor(terrainType);
	TerrainDescriptor_interact(descriptor, terrain, creature, verb);
}

int MapCell_getInventoryCount(MAPCELL* ptr, ITEMTYPE item)
{
	assert(ptr);
	return Inventory_getCount(MapCell_getInventory(ptr), item);
}

void MapCell_changeInventoryCountBy(MAPCELL* ptr, ITEMTYPE item, int delta)
{
	assert(ptr);
	Inventory_setCount(MapCell_getInventory(ptr), item, MapCell_getInventoryCount(ptr, item) + delta);
}

TERRAIN* MapCell_getTerrain(MAPCELL* ptr)
{
	assert(ptr);
	return &(ptr->terrain);

}

CREATURE* MapCell_getCreature(MAPCELL* ptr)
{
	assert(ptr);
	return &(ptr->creature);
}

INVENTORY* MapCell_getInventory(MAPCELL* ptr)
{
	assert(ptr);
	return &(ptr->inventory);
}

