#include "View_internals.h"
#include "MainMenu.h"
#include "StatsView.h"
#include "MessageFeed.h"
#include "QuickHelp.h"
#include "MapScreen.h"
#include "ConfirmQuit.h"
#include <assert.h>

typedef void(*ViewDrawFunc)(VIEW*);
typedef int(*ViewProcessFunc)(VIEW*,VIEWCOMMANDTYPE);

ViewDrawFunc viewDrawFuncs[VIEW_COUNT] = {
	MapScreen_draw,
	StatsView_draw,
	MessageFeed_draw,
	VerbToolbar_draw,
	QuickHelp_draw,
	CraftScreen_draw,
	InventoryScreen_draw,
	MainMenu_draw,
	ConfirmQuit_draw
};

ViewProcessFunc viewProcessFuncs[VIEW_COUNT] = {
	MapScreen_process,
	StatsView_process,
	MessageFeed_process,
	VerbToolbar_process,
	QuickHelp_process,
	CraftScreen_process,
	InventoryScreen_process,
	MainMenu_process,
	ConfirmQuit_process
};

void View_draw(VIEW* ptr)
{
	assert(ptr);
	if (View_isEnabled(ptr))
	{
		viewDrawFuncs[ptr->viewCommon.viewType](ptr);
	}
}

int View_process(VIEW* ptr,VIEWCOMMANDTYPE command)
{
	assert(ptr);
	if (View_isEnabled(ptr))
	{
		return viewProcessFuncs[ptr->viewCommon.viewType](ptr,command);
	}
	else
	{
		return 0;
	}
}

void View_enable(VIEW* ptr)
{
	assert(ptr);
	ViewCommon_enable(View_getCommon(ptr));
}

void View_disable(VIEW* ptr)
{
	assert(ptr);
	ViewCommon_disable(View_getCommon(ptr));
}

int View_isEnabled(VIEW* ptr)
{
	assert(ptr);
	return ViewCommon_isEnabled(View_getCommon(ptr));
}

VIEWTYPE View_getType(VIEW* ptr)
{
	assert(ptr);
	return ViewCommon_getType(View_getCommon(ptr));
}

PIXELPLOTTERSTATE* View_getPixelPlotterState(VIEW* ptr)
{
	assert(ptr);
	return ViewCommon_getPixelPlotterState(View_getCommon(ptr));
}

VIEWCOMMON* View_getCommon(VIEW* ptr)
{
	assert(ptr);
	return &(ptr->viewCommon);
}

CRAFTSCREEN* View_getCraftScreen(VIEW* ptr)
{
	assert(ptr);
	return &(ptr->craftScreen);
}

INVENTORYSCREEN* View_getInventoryScreen(VIEW* ptr)
{
	assert(ptr);
	return &(ptr->inventoryScreen);
}

VERBTOOLBAR* View_getVerbToolbar(VIEW* ptr)
{
	assert(ptr);
	return &(ptr->verbToolbar);
}

