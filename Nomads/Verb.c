#include "Verb_internals.h"
#include <assert.h>
#include "Game.h"
#include "BerryDescriptor_internals.h"
#include "ItemDescriptors.h"
#include "TerrainDescriptors.h"
#include <stdio.h>
#include "VerbDescriptors.h"

void VerbDescriptor_defaultUse(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	Game_writeMessage("Doesn't work!", COLOR_SILVER);
}

void LookVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	MAPCELL* cell = Game_getTagonMapCell();
	TERRAIN* terrain = MapCell_getTerrain(cell);
	TERRAINTYPE terrainType = Terrain_getType(terrain);
	char buffer[MESSAGEFEED_COLUMNS + 1];
	sprintf_s(buffer, MESSAGEFEED_COLUMNS, "You are on %s", TerrainDescriptors_getName(terrainType));
	Game_writeMessage(buffer, COLOR_SILVER);
}

void TakeVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	VerbDescriptor_defaultUse(ptr, tagon);
}

void PickBerryVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	VerbDescriptor_defaultUse(ptr, tagon);
}

void TakeLogVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	VerbDescriptor_defaultUse(ptr, tagon);
}

void TalkVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	VerbDescriptor_defaultUse(ptr, tagon);
}

void AxeVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	VerbDescriptor_defaultUse(ptr, tagon);
}

void PickVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	VerbDescriptor_defaultUse(ptr, tagon);
}

void SpearVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	VerbDescriptor_defaultUse(ptr, tagon);
}

void ShovelVerb_do(TAGON* tagon, TERRAIN* terrain, VERBTYPE verb)
{
	char buffer[MESSAGEFEED_COLUMNS + 1];
	TERRAINTYPE terrainType = Terrain_getType(terrain);
	if (Tagon_getCurrentStatistic(tagon, STATISTIC_ENERGY) < VerbDescriptors_getEnergy(verb))
	{
		Game_writeMessage("Not enough energy!", COLOR_SILVER);
	}
	else if (terrainType == TERRAIN_STUMP)
	{
		Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, -VerbDescriptors_getEnergy(verb));
		Tagon_changeInventoryCountBy(tagon, ITEM_ACORN, 1);
		Tagon_changeInventoryCountBy(tagon, VerbDescriptors_getItem(verb), -1);
		TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_DIRT), terrain);

		sprintf_s(buffer, MESSAGEFEED_COLUMNS, "+1 %s (%d)", ItemDescriptors_getName(ITEM_ACORN), Tagon_getInventoryCount(tagon, ITEM_ACORN));
		Game_writeMessage(buffer, COLOR_SILVER);
	}
	else if (terrainType == TERRAIN_RUBBLE)
	{
		Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, -VerbDescriptors_getEnergy(verb));
		Tagon_changeInventoryCountBy(tagon, ITEM_GRAVEL, 1);
		Tagon_changeInventoryCountBy(tagon, VerbDescriptors_getItem(verb), -1);
		TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_DIRT), terrain);

		sprintf_s(buffer, MESSAGEFEED_COLUMNS, "+1 %s (%d)", ItemDescriptors_getName(ITEM_GRAVEL), Tagon_getInventoryCount(tagon, ITEM_GRAVEL));
		Game_writeMessage(buffer, COLOR_SILVER);
	}
}

void ShovelVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	char buffer[MESSAGEFEED_COLUMNS + 1];
	ITEMDESCRIPTOR* itemDescriptor = ItemDescriptors_getDescriptor(VerbDescriptor_getItem(ptr));
	ITEMTYPE itemType = ItemDescriptor_getType(itemDescriptor);
	MAPCELL* cell = Game_getTagonMapCell();
	TERRAIN* terrain = MapCell_getTerrain(cell);
	TERRAINTYPE terrainType = Terrain_getType(terrain);
	if (terrainType == TERRAIN_STUMP || terrainType == TERRAIN_RUBBLE)
	{
		ShovelVerb_do(tagon, terrain, VerbDescriptor_getType(ptr));
	}
	else
	{
		VerbDescriptor_defaultUse(ptr, tagon);
	}
}

void PlantSeedVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	VerbDescriptor_defaultUse(ptr, tagon);
}

void PlantAcornVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	char buffer[MESSAGEFEED_COLUMNS + 1];
	ITEMDESCRIPTOR* itemDescriptor = ItemDescriptors_getDescriptor(VerbDescriptor_getItem(ptr));
	ITEMTYPE itemType = ItemDescriptor_getType(itemDescriptor);
	MAPCELL* cell = Game_getTagonMapCell();
	TERRAIN* terrain = MapCell_getTerrain(cell);
	TERRAINTYPE terrainType = Terrain_getType(terrain);
	if (terrainType == TERRAIN_GRASS || terrainType == TERRAIN_DIRT)
	{
		if (Tagon_getCurrentStatistic(tagon, STATISTIC_ENERGY) < VerbDescriptor_getEnergy(ptr))
		{
			Game_writeMessage("Not enough energy!", COLOR_SILVER);
		}
		else
		{
			Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, VerbDescriptor_getEnergy(ptr));
			Tagon_changeInventoryCountBy(tagon, itemType, -1);
			TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_ACORN), MapCell_getTerrain(cell));

			sprintf_s(buffer, MESSAGEFEED_COLUMNS, "-1 %s (%d)", ItemDescriptors_getName(ItemDescriptor_getType(itemDescriptor)), Tagon_getInventoryCount(tagon, ItemDescriptor_getType(itemDescriptor)));
			Game_writeMessage(buffer, COLOR_SILVER);
		}
	}
	else
	{
		VerbDescriptor_defaultUse(ptr, tagon);
	}
}

void PlaceGravelVerb_do(TAGON* tagon, TERRAIN* terrain)
{
	assert(tagon);
	assert(terrain);
	VERBTYPE verb = VERB_PLACE_GRAVEL;
	char buffer[MESSAGEFEED_COLUMNS + 1];
	if (Tagon_getCurrentStatistic(tagon, STATISTIC_ENERGY) < VerbDescriptors_getEnergy(verb))
	{
		Game_writeMessage("Not enough energy!", COLOR_SILVER);
	}
	else
	{
		Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, -1);
		Tagon_changeInventoryCountBy(tagon, VerbDescriptors_getItem(verb), -1);
		TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_RUBBLE), terrain);

		sprintf_s(buffer, MESSAGEFEED_COLUMNS, "-1 %s (%d)", ItemDescriptors_getName(VerbDescriptors_getItem(verb)), Tagon_getInventoryCount(tagon, VerbDescriptors_getItem(verb)));
		Game_writeMessage(buffer, COLOR_SILVER);
	}
}

void PlaceGravelVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	MAPCELL* cell = Game_getTagonMapCell();
	TERRAIN* terrain = MapCell_getTerrain(cell);
	TERRAINTYPE terrainType = Terrain_getType(terrain);
	if (terrainType == TERRAIN_GRASS || terrainType == TERRAIN_DIRT)
	{
		PlaceGravelVerb_do(tagon, terrain);
	}
	else
	{
		VerbDescriptor_defaultUse(ptr, tagon);
	}
}

void EatBerryVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	assert(tagon);
	char buffer[MESSAGEFEED_COLUMNS + 1];
	ITEMDESCRIPTOR* itemDescriptor = ItemDescriptors_getDescriptor(VerbDescriptor_getItem(ptr));
	BERRYDESCRIPTOR* berry = ItemDescriptor_getBerry(itemDescriptor);
	//remove berry
	Tagon_changeInventoryCountBy(tagon, ItemDescriptor_getType(itemDescriptor), -1);
	//add energy
	Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, BerryDescriptor_getEnergy(berry));

	sprintf_s(buffer,MESSAGEFEED_COLUMNS, "+%d Energy (%d)", BerryDescriptor_getEnergy(berry), Tagon_getCurrentStatistic(tagon,STATISTIC_ENERGY));
	Game_writeMessage(buffer,COLOR_SILVER);

	sprintf_s(buffer, MESSAGEFEED_COLUMNS, "-1 %s (%d)", ItemDescriptor_getName(itemDescriptor), Tagon_getInventoryCount(tagon, ItemDescriptor_getType(itemDescriptor)));
	Game_writeMessage(buffer, COLOR_SILVER);
}

