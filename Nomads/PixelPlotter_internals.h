#ifndef PIXEL_PLOTTER_INTERNALS_H
#define PIXEL_PLOTTER_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "PixelPlotter.h"

	struct PixelPlotterState{
		int offsetX;
		int offsetY;
		int scaleX;
		int scaleY;
		int clipEnabled;
		int clipWidth;
		int clipHeight;
	};

#ifdef __cplusplus
}
#endif

#endif

