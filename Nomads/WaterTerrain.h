#ifndef WATER_TERRAIN_H
#define WATER_TERRAIN_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct WaterTerrain WATERTERRAIN;

	int WaterTerrain_getFishLeft(WATERTERRAIN* ptr);
	int WaterTerrain_hasFishLeft(WATERTERRAIN* ptr);
	void WaterTerrain_setFishLeft(WATERTERRAIN* ptr, int fishLeft);
	void WaterTerrain_changeFishLeftBy(WATERTERRAIN* ptr, int delta);

	int WaterTerrain_getMaximumFish(WATERTERRAIN* ptr);
	int WaterTerrain_getRestockTimer(WATERTERRAIN* ptr);


#ifdef __cplusplus
}
#endif

#endif

