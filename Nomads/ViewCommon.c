#include "ViewCommon_internals.h"
#include <assert.h>

void ViewCommon_enable(VIEWCOMMON* ptr)
{
	assert(ptr);
	ptr->enabled = !0;
}

void ViewCommon_disable(VIEWCOMMON* ptr)
{
	assert(ptr);
	ptr->enabled = 0;
}

int ViewCommon_isEnabled(VIEWCOMMON* ptr)
{
	assert(ptr);
	return ptr->enabled;
}

VIEWTYPE ViewCommon_getType(VIEWCOMMON* ptr)
{
	assert(ptr);
	return ptr->viewType;
}

PIXELPLOTTERSTATE* ViewCommon_getPixelPlotterState(VIEWCOMMON* ptr)
{
	assert(ptr);
	return &(ptr->pixelPlotterState);
}


