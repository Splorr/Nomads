#include "Game.h"
#include "ItemDescriptors.h"
#include "TerrainDescriptors.h"
#include "CreatureDescriptors.h"
#include "VerbDescriptors.h"
#include "Direction.h"
#include "World_internals.h"
#include "Recipes.h"
#include <assert.h>

static WORLD world;

void Game_initialize(void)
{
	Recipes_initialize();
	CreatureDescriptors_initialize();
	TerrainDescriptors_initialize();
	ItemDescriptors_initialize();
	VerbDescriptors_initialize();
	World_initialize(&world);
}

WORLD* Game_getWorld(void)
{
	return &world;
}

void Game_moveTagon(DIRECTIONTYPE direction)
{
	assert(direction >= DIRECTION_FIRST && direction < DIRECTION_COUNT);
	int x = Avatar_getX(World_getAvatar(Game_getWorld()));
	int y = Avatar_getY(World_getAvatar(Game_getWorld()));
	MAPCELL* cell = Map_getMapCell(World_getMap(Game_getWorld()),x,y);
	CREATURE* creature = MapCell_getCreature(cell);

	int nextX = Direction_getNextX(x, y, direction);
	int nextY = Direction_getNextY(x, y, direction);
	MAPCELL* nextCell = Map_getMapCell(World_getMap(Game_getWorld()), nextX, nextY);

	if (MapCell_canEnter(nextCell, creature))
	{
		Tagon_setExertion(Creature_getTagon(creature), EXERTION_LOW);
		CREATURE* nextCreature = MapCell_getCreature(nextCell);
		Creature_swap(creature, nextCreature);
		Avatar_move(World_getAvatar(Game_getWorld()),direction);
	}
	else
	{
		Tagon_setExertion(Creature_getTagon(creature), EXERTION_NONE);
		Avatar_setFacing(World_getAvatar(Game_getWorld()), direction);
	}
	Game_doTimers();
}

void Game_doTimers(void)
{
	World_doTimers(Game_getWorld());
}

TAGON* Game_getTagon(void)
{
	return World_getTagon(Game_getWorld());
}

INVENTORY* Game_getGroundInventory(void)
{
	return World_getGroundInventory(Game_getWorld());
}

void Game_tagonInteract(void)
{
	int x = Avatar_getX(World_getAvatar(Game_getWorld()));
	int y = Avatar_getY(World_getAvatar(Game_getWorld()));
	DIRECTIONTYPE direction = Avatar_getFacing(World_getAvatar(Game_getWorld()));
	MAPCELL* cell = Map_getMapCell(World_getMap(Game_getWorld()),x,y);
	CREATURE* creature = MapCell_getCreature(cell);

	int nextX = Direction_getNextX(x, y, direction);
	int nextY = Direction_getNextY(x, y, direction);
	MAPCELL* nextCell = Map_getMapCell(World_getMap(Game_getWorld()), nextX, nextY);

	TAGON* tagon = Creature_getTagon(creature);
	VERBTYPE verb = Tagon_getVerb(tagon);

	Tagon_setExertion(tagon, EXERTION_NONE);//set to none, and let interaction figure out exertion level
	//determine energy cost
	if (Statistic_getCurrent(Tagon_getStatistic(tagon, STATISTIC_ENERGY)) >= VerbDescriptors_getEnergy(verb))
	{
		if (MapCell_canInteract(nextCell, creature, verb))
		{
			MapCell_interact(nextCell, creature, verb);
			Creature_postInteract(creature);
		}
		Game_doTimers();
	}
	else
	{
		Game_writeMessage("Not enough energy!", COLOR_SILVER);
	}
}

void Game_tagonUseVerb(void)
{
	int x = Avatar_getX(World_getAvatar(Game_getWorld()));
	int y = Avatar_getY(World_getAvatar(Game_getWorld()));
	DIRECTIONTYPE direction = Avatar_getFacing(World_getAvatar(Game_getWorld()));
	MAPCELL* cell = Map_getMapCell(World_getMap(Game_getWorld()), x, y);
	CREATURE* creature = MapCell_getCreature(cell);
	TAGON* tagon = Creature_getTagon(creature);
	Tagon_setExertion(tagon, EXERTION_NONE);//default to none, and let the verb figure it out

	Tagon_useVerb(tagon, Tagon_getVerb(tagon));
	Creature_postInteract(creature);
	Game_doTimers();
}

void Game_writeMessage(const char* message, COLORTYPE color)
{
	World_writeMessage(Game_getWorld(), message, color);
}

MAPCELL* Game_getTagonMapCell(void)
{
	int x = Avatar_getX(World_getAvatar(Game_getWorld()));
	int y = Avatar_getY(World_getAvatar(Game_getWorld()));
	DIRECTIONTYPE direction = Avatar_getFacing(World_getAvatar(Game_getWorld()));
	return Map_getMapCell(World_getMap(Game_getWorld()), x, y);
}
