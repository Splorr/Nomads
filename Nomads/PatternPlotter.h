#ifndef PATTERN_PLOTTER_H
#define PATTERN_PLOTTER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "PatternType.h"
#include "ColorType.h"

void PatternPlotter_plotPattern(int x, int y, PATTERNTYPE pattern, COLORTYPE foreground, COLORTYPE background);

#ifdef __cplusplus
}
#endif

#endif


