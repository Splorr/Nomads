#ifndef DIRT_TERRAIN_INTERNALS_H
#define DIRT_TERRAIN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DirtTerrain.h"
#include "TerrainCommon_internals.h"

	struct DirtTerrain {
		TERRAINCOMMON common;
		int grassTimer;
	};

	union TerrainDescriptor;
	union Terrain;
	struct MapCell;

	void DirtTerrain_doTimer(union TerrainDescriptor* ptr, struct MapCell* cell);
	void DirtTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);

#ifdef __cplusplus
}
#endif

#endif

