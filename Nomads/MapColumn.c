#include "MapColumn_internals.h"
#include "Utility.h"
#include <assert.h>


MAPCELL* MapColumn_getMapCell(MAPCOLUMN* ptr, int row)
{
	assert(ptr);
	return &(ptr->cells[Utility_modulo(row,MAP_ROWS)]);
}

void MapColumn_doTimers(MAPCOLUMN* ptr)
{
	assert(ptr);
	for(int row=0;row<MAP_ROWS;++row)
	{
		MapCell_doTimers(MapColumn_getMapCell(ptr,row));
	}
}

