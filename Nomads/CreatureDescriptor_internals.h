#ifndef CREATURE_DESCRIPTOR_INTERNALS_H
#define CREATURE_DESCRIPTOR_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "CreatureDescriptor.h"
#include "DescriptorCommon_internals.h"

union CreatureDescriptor {
	DESCRIPTORCOMMON common;
};

DESCRIPTORCOMMON* CreatureDescriptor_getCommon(CREATUREDESCRIPTOR* ptr);

#ifdef __cplusplus
}
#endif

#endif
