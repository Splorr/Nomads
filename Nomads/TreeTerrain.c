#include "TreeTerrain_internals.h"
#include "Utility.h"
#include <assert.h>
#include "Generators.h"
#include "TerrainDescriptor.h"
#include "Terrain.h"
#include "MapCell.h"
#include "Terrain_internals.h"

void TreeTerrain_setWoodLeft(TREETERRAIN* ptr, int woodLeft)
{
	assert(ptr);
	ptr->woodLeft = Utility_maximum(woodLeft,0);
}

void TreeTerrain_changeWoodLeftBy(TREETERRAIN* ptr, int delta)
{
	assert(ptr);
	TreeTerrain_setWoodLeft(ptr, TreeTerrain_getWoodLeft(ptr) + delta);
}

int TreeTerrain_getWoodLeft(TREETERRAIN* ptr)
{
	assert(ptr);
	return ptr->woodLeft;
}

int TreeTerrain_getAcornTimer(TREETERRAIN* ptr)
{
	assert(ptr);
	return ptr->acornTimer;
}

int TreeTerrain_hasWoodLeft(TREETERRAIN* ptr)
{
	assert(ptr);
	return TreeTerrain_getWoodLeft(ptr) > 0;
}

void TreeTerrain_setAcornTimer(TREETERRAIN* ptr, int acornTimer)
{
	assert(ptr);
	ptr->acornTimer = acornTimer;
}

void TreeTerrain_changeAcornTimerBy(TREETERRAIN* ptr, int delta)
{
	assert(ptr);
	TreeTerrain_setAcornTimer(ptr, TreeTerrain_getAcornTimer(ptr) + delta);
}

void TreeTerrain_doTimer(union TerrainDescriptor* ptr, struct MapCell* cell)
{
	TREETERRAIN* tree = Terrain_getTree(MapCell_getTerrain(cell));
	TreeTerrain_changeAcornTimerBy(tree, -1);
	if (TreeTerrain_getAcornTimer(tree) <= 0)
	{
		//TODO: generate adjacent acorn
		//find x and y of cell
		//pick a random direction
		//find neighbor cell
		//if neighbor cell is grass and without items or creature, create acorn terrain
		//reset timer
		TreeTerrain_setAcornTimer(tree, Generator_generate(Generators_getGenerator(GENERATOR_ACORNTIMER)));
	}
}

void TreeTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_TREE;
	ptr->tree.woodLeft = Generator_generate(Generators_getGenerator(GENERATOR_WOODLEFT));
	ptr->tree.acornTimer = Generator_generate(Generators_getGenerator(GENERATOR_ACORNTIMER));
}

