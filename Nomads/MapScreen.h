#ifndef MAP_SCREEN_H
#define MAP_SCREEN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewCommon.h"
#include "ViewCommandType.h"

typedef struct MapScreen MAPSCREEN;

union View;

void MapScreen_draw(union View*);
int MapScreen_process(union View*, VIEWCOMMANDTYPE);
void MapScreen_initialize(union View*);

#ifdef __cplusplus
}
#endif

#endif

