#include "Generators.h"
#include "TerrainType.h"
#include "BerryType.h"
#include <assert.h>

static GENERATORENTRY terrainTypeGeneratorEntries[] = 
{
	{ TERRAIN_GRASS,8000 },
	{ TERRAIN_TREE,2000 },
	{ TERRAIN_ROCK,500 },
	{ TERRAIN_WATER,250 },
	{ TERRAIN_FLINT_ROCK,50 },
	{ TERRAIN_BUSH,25 },
	{ TERRAIN_RABBIT_HOLE,10 }
};

static GENERATORENTRY woodLeftGeneratorEntries[] = 
{
	{ 10,1 }
};

static GENERATORENTRY stoneLeftGeneratorEntries[] =
{
	{ 10,1 }
};

static GENERATORENTRY flintLeftGeneratorEntries[] =
{
	{ 5,1 }
};

static GENERATORENTRY acornTimerGeneratorEntries[] =
{
	{60,1},
	{120,2},
	{180,3},
	{240,4},
	{200,5},
	{360,6},
	{420,6},
	{480,5},
	{540,4},
	{600,3},
	{660,2},
	{720,1}
};

static GENERATORENTRY berryTimerGeneratorEntries[] =
{
	{ 30,1 },
	{ 60,4 },
	{ 150,9 }
};

static GENERATORENTRY berryDeathTimerGeneratorEntries[] =
{
{ 300,1 },
{ 600,3 },
{ 900,3 },
{ 1200,1 }
};

static GENERATORENTRY berryTypeGeneratorEntries[] =
{
	{BERRY_CARNELIAN,200},
	{BERRY_COPPER,100},
	{BERRY_GOLD,50},
	{BERRY_JADE,20},
	{BERRY_TURQUOISE,10},
	{ BERRY_RUBY, 5},
	{ BERRY_AMETHYST,2},
	{BERRY_SILVER,1}
};

static GENERATORENTRY fishLeftGeneratorEntries[] =
{
	{ 10,1 }
};

static GENERATORENTRY maximumFishGeneratorEntries[] =
{
	{ 20,1 }
};

static GENERATORENTRY restockTimerGeneratorEntries[] =
{
	{ 1000,1 }
};

static GENERATORENTRY berryStarterGrowthTimerGeneratorEntries[] =
{
{ 150,1 },
{ 300,3 },
{ 450,2 }
};

static GENERATORENTRY dirtToGrassGeneratorEntries[] =
{
	{ 30,1 },
{ 60,2 },
{ 90,3 },
{ 120,3 },
{ 150,2 },
{ 180,1 }
};

static GENERATORENTRY acornToTreeGeneratorEntries[] =
{
	{ 300,1 },
{ 600,2 },
{ 900,3 },
{ 1200,3 },
{ 1500,2 },
{ 1800,1 }
};

static GENERATOR generators[GENERATOR_COUNT] =
{
	{sizeof(terrainTypeGeneratorEntries)/sizeof(GENERATORENTRY),terrainTypeGeneratorEntries},
	{sizeof(woodLeftGeneratorEntries)/sizeof(GENERATORENTRY),woodLeftGeneratorEntries},
	{sizeof(acornTimerGeneratorEntries)/sizeof(GENERATORENTRY),acornTimerGeneratorEntries},
	{sizeof(berryTimerGeneratorEntries) / sizeof(GENERATORENTRY),berryTimerGeneratorEntries},
	{sizeof(berryDeathTimerGeneratorEntries) / sizeof(GENERATORENTRY),berryDeathTimerGeneratorEntries},
	{sizeof(berryTypeGeneratorEntries) / sizeof(GENERATORENTRY),berryTypeGeneratorEntries},
{ sizeof(stoneLeftGeneratorEntries) / sizeof(GENERATORENTRY),stoneLeftGeneratorEntries },
{ sizeof(flintLeftGeneratorEntries) / sizeof(GENERATORENTRY),flintLeftGeneratorEntries },
{ sizeof(fishLeftGeneratorEntries) / sizeof(GENERATORENTRY),fishLeftGeneratorEntries },
{ sizeof(maximumFishGeneratorEntries) / sizeof(GENERATORENTRY),maximumFishGeneratorEntries },
{ sizeof(restockTimerGeneratorEntries) / sizeof(GENERATORENTRY),restockTimerGeneratorEntries },
{ sizeof(berryStarterGrowthTimerGeneratorEntries) / sizeof(GENERATORENTRY),berryStarterGrowthTimerGeneratorEntries },
{ sizeof(dirtToGrassGeneratorEntries) / sizeof(GENERATORENTRY),dirtToGrassGeneratorEntries },
{ sizeof(acornToTreeGeneratorEntries) / sizeof(GENERATORENTRY),acornToTreeGeneratorEntries }
};

GENERATOR* Generators_getGenerator(GENERATORTYPE generatorType)
{
	assert(generatorType >= GENERATOR_FIRST && generatorType < GENERATOR_COUNT);
	return &generators[generatorType];
}
