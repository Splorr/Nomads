#include "SDL.h"

#include "Template.h"
#include "PixelPlotter.h"
#include "TextPlotter.h"
#include "Views.h"
#include "Colors.h"
#include "Constants.h"
#include <stdlib.h>
#include <time.h>
#include <conio.h>

int main(int argc, char** argv)
{
	srand((unsigned int)time(0));
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window* window = 0;
	SDL_Renderer* renderer = 0;

	//SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_FULLSCREEN_DESKTOP, &window, &renderer);
	SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, 0, &window, &renderer);
	SDL_RenderSetLogicalSize(renderer,SCREEN_WIDTH,SCREEN_HEIGHT);

	PixelPlotter_setRenderer(renderer);
	PixelPlotter_setScale(SCALE_SMALL, SCALE_SMALL);

	SDL_GameController *controller = NULL;
	for (int i = 0; i < SDL_NumJoysticks(); ++i) {
		if (SDL_IsGameController(i)) {
			controller = SDL_GameControllerOpen(i);
			if (controller) {
				break;
			}
			else {
				SDL_Log("Could not open gamecontroller %i: %s\n", i, SDL_GetError());
			}
		}
	}


	Views_initialize();

	SDL_Event evt;

	while(Views_countEnabled()>0)
	{
		if (SDL_PollEvent(&evt))
		{
			if (evt.type == SDL_QUIT)
			{
				break;
			}
			else
			{
				switch (evt.type)
				{
				case SDL_CONTROLLERBUTTONDOWN:
					switch (evt.cbutton.button)
					{
					case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
						Views_process(VIEWCOMMAND_MOVE_DOWN);
						break;
					case SDL_CONTROLLER_BUTTON_DPAD_UP:
						Views_process(VIEWCOMMAND_MOVE_UP);
						break;
					case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
						Views_process(VIEWCOMMAND_MOVE_LEFT);
						break;
					case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
						Views_process(VIEWCOMMAND_MOVE_RIGHT);
						break;
					case SDL_CONTROLLER_BUTTON_A:
						Views_process(VIEWCOMMAND_BUTTON_A);
						break;
					case SDL_CONTROLLER_BUTTON_B:
						Views_process(VIEWCOMMAND_BUTTON_B);
						break;
					case SDL_CONTROLLER_BUTTON_X:
						Views_process(VIEWCOMMAND_BUTTON_X);
						break;
					case SDL_CONTROLLER_BUTTON_Y:
						Views_process(VIEWCOMMAND_BUTTON_Y);
						break;
					case SDL_CONTROLLER_BUTTON_BACK:
						Views_process(VIEWCOMMAND_BUTTON_BACK);
						break;
					case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
						Views_process(VIEWCOMMAND_BUTTON_PREVIOUS);
						break;
					case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
						Views_process(VIEWCOMMAND_BUTTON_NEXT);
						break;
					case SDL_CONTROLLER_BUTTON_START:
						Views_process(VIEWCOMMAND_BUTTON_START);
						break;
					}
					break;
				case SDL_KEYDOWN: 
					switch (evt.key.keysym.sym)
					{
					case SDLK_w:
						Views_process(VIEWCOMMAND_MOVE_UP);
						break;
					case SDLK_s:
						Views_process(VIEWCOMMAND_MOVE_DOWN);
						break;
					case SDLK_a:
						Views_process(VIEWCOMMAND_MOVE_LEFT);
						break;
					case SDLK_d:
						Views_process(VIEWCOMMAND_MOVE_RIGHT);
						break;
					case SDLK_e:
					case SDLK_SPACE:
						Views_process(VIEWCOMMAND_BUTTON_A);
						break;
					case SDLK_q:
						Views_process(VIEWCOMMAND_BUTTON_B);
						break;
					case SDLK_c:
						Views_process(VIEWCOMMAND_BUTTON_Y);
						break;
					case SDLK_x:
						Views_process(VIEWCOMMAND_BUTTON_X);
						break;
					case SDLK_f:
						Views_process(VIEWCOMMAND_BUTTON_BACK);
						break;
					case SDLK_PERIOD:
						Views_process(VIEWCOMMAND_BUTTON_NEXT);
						break;
					case SDLK_COMMA:
						Views_process(VIEWCOMMAND_BUTTON_PREVIOUS);
						break;
					case SDLK_ESCAPE:
						Views_process(VIEWCOMMAND_BUTTON_START);
						break;
					}
					break;
				}
			}
		}
		else
		{
			COLOR* color = Colors_getColor(COLOR_ONYX);
			SDL_SetRenderDrawColor(renderer, Color_getRed(color), Color_getGreen(color), Color_getBlue(color), Color_getRed(color));
			SDL_RenderClear(renderer);

			Views_draw();

			SDL_RenderPresent(renderer);
		}
	}

	if (controller)
	{
		SDL_GameControllerClose(controller);
		controller = 0;
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	SDL_Quit();

    return 0;
}

