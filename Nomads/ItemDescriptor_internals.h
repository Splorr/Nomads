#ifndef ITEM_DESCRIPTOR_INTERNALS_H
#define ITEM_DESCRIPTOR_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ItemDescriptor.h"
#include "ItemDescriptorCommon_internals.h"
#include "BerryDescriptor_internals.h"
#include "ShovelDescriptor_internals.h"
#include "GravelDescriptor_internals.h"
#include "AcornDescriptor_internals.h"

union ItemDescriptor {
	ITEMDESCRIPTORCOMMON itemCommon;
	BERRYDESCRIPTOR berry;
};

ITEMDESCRIPTORCOMMON* ItemDescriptor_getItemCommon(ITEMDESCRIPTOR* ptr);
void ItemDescriptor_doNothingUse(union ItemDescriptor* descriptor, struct Tagon* tagon, VERBTYPE verb);

#ifdef __cplusplus
}
#endif

#endif

