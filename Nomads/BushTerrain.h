#ifndef BERRY_TERRAIN_H
#define BERRY_TERRAIN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "BerryType.h"
#include "TerrainType.h"

typedef struct BushTerrain BUSHTERRAIN;

TERRAINTYPE BushTerrain_getType(BUSHTERRAIN* ptr);
BERRYTYPE BushTerrain_getBerry(BUSHTERRAIN* ptr);
int BushTerrain_getBerryTimer(BUSHTERRAIN* ptr);
int BushTerrain_getDeathTimer(BUSHTERRAIN* ptr);
void BushTerrain_setBerry(BUSHTERRAIN* ptr, BERRYTYPE berry);
void BushTerrain_setBerryTimer(BUSHTERRAIN* ptr, int value);
void BushTerrain_setDeathTimer(BUSHTERRAIN* ptr, int value);
void BushTerrain_changeBerryTimerBy(BUSHTERRAIN* ptr, int delta);
void BushTerrain_changeDeathTimerBy(BUSHTERRAIN* ptr, int delta);



#ifdef __cplusplus
}
#endif

#endif

