#include "TextPlotter.h"
#include "PatternPlotter.h"
#include "Pattern.h"
#include <string.h>
#include <assert.h>

void TextPlotter_plotText(int x, int y, const char* text, COLORTYPE foreground, COLORTYPE background)
{
	assert(text);
	assert(foreground >= COLOR_FIRST && foreground <= COLOR_COUNT);
	assert(background >= COLOR_FIRST && background <= COLOR_COUNT);
	for (const char* s = text; *s; ++s, x+= PATTERN_WIDTH)
	{
		PatternPlotter_plotPattern(x, y, (PATTERNTYPE)(unsigned char)(*s), foreground, background);
	}
}

void TextPlotter_centerText(int x, int y, const char* text, COLORTYPE foreground, COLORTYPE background)
{
	assert(text);
	assert(foreground >= COLOR_FIRST && foreground <= COLOR_COUNT);
	assert(background >= COLOR_FIRST && background <= COLOR_COUNT);
	TextPlotter_plotText(x - (int)strlen(text) * PATTERN_WIDTH / 2, y, text, foreground, background);
}