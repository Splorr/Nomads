#ifndef ROCK_TERRAIN_H
#define ROCK_TERRAIN_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct RockTerrain ROCKTERRAIN;

	int RockTerrain_getStoneLeft(ROCKTERRAIN* ptr);
	int RockTerrain_hasStoneLeft(ROCKTERRAIN* ptr);
	void RockTerrain_setStoneLeft(ROCKTERRAIN* ptr, int stoneLeft);
	void RockTerrain_changeStoneLeftBy(ROCKTERRAIN* ptr, int delta);

#ifdef __cplusplus
}
#endif

#endif

