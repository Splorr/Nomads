#ifndef TERRAIN_DESCRIPTOR_H
#define TERRAIN_DESCRIPTOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TerrainType.h"
#include "CreatureType.h"
#include "Creature.h"

typedef union TerrainDescriptor TERRAINDESCRIPTOR;

struct MapCell;

int TerrainDescriptor_canSpawnCreature(TERRAINDESCRIPTOR* ptr, CREATURETYPE creatureType);
int TerrainDescriptor_canEnter(TERRAINDESCRIPTOR* ptr, CREATURE* creature);
int TerrainDescriptor_canInteract(TERRAINDESCRIPTOR* ptr, CREATURE* creature, VERBTYPE verb);
void TerrainDescriptor_doTimers(TERRAINDESCRIPTOR* ptr, struct MapCell* cell);

union Terrain;

void TerrainDescriptor_initializeInstance(TERRAINDESCRIPTOR* descriptor, union Terrain* ptr);
void TerrainDescriptor_interact(TERRAINDESCRIPTOR* ptr, union Terrain* terrain, CREATURE* creature, VERBTYPE verb);

TERRAINTYPE TerrainDescriptor_getType(TERRAINDESCRIPTOR* ptr);
PATTERNTYPE TerrainDescriptor_getPattern(TERRAINDESCRIPTOR* ptr);
COLORTYPE TerrainDescriptor_getForeground(TERRAINDESCRIPTOR* ptr);
COLORTYPE TerrainDescriptor_getBackground(TERRAINDESCRIPTOR* ptr);
const char* TerrainDescriptor_getName(TERRAINDESCRIPTOR* ptr);

#ifdef __cplusplus
}
#endif

#endif

