#include "CreatureDescriptors.h"
#include "CreatureDescriptor_internals.h"
#include <assert.h>

static CREATUREDESCRIPTOR creatureDescriptors[CREATURE_COUNT + 1] = { 0 };

typedef void(*CreatureDescriptorInitializeFunc)(CREATUREDESCRIPTOR*);

static void initializeTagon(CREATUREDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.creatureType = CREATURE_TAGON;
	ptr->common.pattern = PATTERN_TAGON;
	ptr->common.foreground = COLOR_MEDIUM;
	ptr->common.background = COLOR_TRANSPARENT;
}

static void initializeNone(CREATUREDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.creatureType = CREATURE_NONE;
	ptr->common.pattern = PATTERN_EMPTY;
	ptr->common.foreground = COLOR_TRANSPARENT;
	ptr->common.background = COLOR_TRANSPARENT;
}

static CreatureDescriptorInitializeFunc creatureDescriptorInitializeFuncs[CREATURE_COUNT+1] =
{
	initializeTagon,
	initializeNone
};

void CreatureDescriptors_initialize(void)
{
	for (int index = 0; index < CREATURE_COUNT + 1 ; ++index)
	{
		creatureDescriptorInitializeFuncs[index](CreatureDescriptors_getDescriptor(index));
	}
}

CREATUREDESCRIPTOR* CreatureDescriptors_getDescriptor(CREATURETYPE creatureType)
{
	assert(creatureType>=CREATURE_FIRST && creatureType<=CREATURE_COUNT);
	return &(creatureDescriptors[creatureType]);
}
