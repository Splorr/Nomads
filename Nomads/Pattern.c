#include "Pattern.h"
#include <assert.h>

int Pattern_getPixel(PATTERN* ptr, int x, int y)
{
	assert(ptr);
	return (ptr->lines[y] & (1 << x)) != 0;
}
