#include "RockTerrain_internals.h"
#include <assert.h>
#include "Terrain_internals.h"
#include "Generators.h"

int RockTerrain_getStoneLeft(ROCKTERRAIN* ptr)
{
	assert(ptr);
	return ptr->stoneLeft;
}

int RockTerrain_hasStoneLeft(ROCKTERRAIN* ptr)
{
	assert(ptr);
	return RockTerrain_getStoneLeft(ptr) > 0;
}

void RockTerrain_setStoneLeft(ROCKTERRAIN* ptr, int stoneLeft)
{
	assert(ptr);
	ptr->stoneLeft = stoneLeft;
}

void RockTerrain_changeStoneLeftBy(ROCKTERRAIN* ptr, int delta)
{
	assert(ptr);
	RockTerrain_setStoneLeft(ptr, RockTerrain_getStoneLeft(ptr) + delta);
}

void RockTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_ROCK;
	ptr->rock.stoneLeft = Generator_generate(Generators_getGenerator(GENERATOR_STONELEFT));
}

