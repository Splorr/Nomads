#include "Colors.h"
#include "Color_internal.h"
#include <assert.h>

static COLOR colors[COLOR_COUNT+1] = {
	{0x20, 0x20, 0x20, 0xFF},
	{0x80, 0x20, 0x20, 0xFF},
	{0x80, 0x40, 0x20, 0xFF},
	{0x80, 0x80, 0x20, 0xFF},
	{0x20, 0x80, 0x20, 0xFF},
	{0x20, 0x80, 0x80, 0xFF},
	{0x20, 0x20, 0x80, 0xFF},
	{0x80, 0x20, 0x80, 0xFF},
	{0x80, 0x80, 0x80, 0xFF},
	{0x68, 0x20, 0x20, 0xFF},
	{0x68, 0x38, 0x20, 0xFF},
	{0x68, 0x68, 0x20, 0xFF},
	{0x20, 0x68, 0x20, 0xFF},
	{0x20, 0x68, 0x68, 0xFF},
	{0x20, 0x20, 0x68, 0xFF},
	{0x68, 0x20, 0x68, 0xFF},
	{0x68, 0x68, 0x68, 0xFF},
	{0x50, 0x20, 0x20, 0xFF},
	{0x50, 0x30, 0x20, 0xFF},
	{0x50, 0x50, 0x20, 0xFF},
	{0x20, 0x50, 0x20, 0xFF},
	{0x20, 0x50, 0x50, 0xFF},
	{0x20, 0x20, 0x50, 0xFF},
	{0x50, 0x20, 0x50, 0xFF},
	{0x50, 0x50, 0x50, 0xFF},
	{0x38, 0x20, 0x20, 0xFF},
	{0x38, 0x28, 0x20, 0xFF},
	{0x38, 0x38, 0x20, 0xFF},
	{0x20, 0x38, 0x20, 0xFF},
	{0x20, 0x38, 0x38, 0xFF},
	{0x20, 0x20, 0x38, 0xFF},
	{0x38, 0x20, 0x38, 0xFF},
	{0x38, 0x38, 0x38, 0xFF},
	{0xFF, 0xFF, 0xFF, 0xFF},
	{0xC0, 0xC0, 0xC0, 0xFF},
	{0xFF, 0xCC, 0x88, 0xFF},
	{0xF0, 0xC0, 0x80, 0xFF},
	{0xE1, 0xB4, 0x78, 0xFF},
	{0xD2, 0xA8, 0x70, 0xFF},
	{0xC3, 0x9C, 0x68, 0xFF},
	{0xB4, 0x90, 0x60, 0xFF},
	{0xA5, 0x84, 0x58, 0xFF},
	{0x96, 0x78, 0x50, 0xFF},
	{0x87, 0x6C, 0x48, 0xFF},
	{0x78, 0x60, 0x40, 0xFF},
	{ 0x69, 0x54, 0x38, 0xFF },
	{ 0,0,0,0 }
};

COLOR* Colors_getColor(COLORTYPE colorType)
{
	assert(colorType >= COLOR_FIRST && colorType <= COLOR_COUNT);
	return &colors[colorType];
}

