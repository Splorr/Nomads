#include "Creature_internals.h"
#include "CreatureDescriptor.h"
#include "CreatureDescriptors.h"
#include <string.h>
#include "Statistic.h"
#include <assert.h>

void Creature_initialize(CREATURE* ptr, CREATUREDESCRIPTOR* descriptor)
{
	assert(ptr);
	assert(descriptor);
	CreatureDescriptor_initializeInstance(descriptor,ptr);
}

void Creature_clear(CREATURE* ptr)
{
	assert(ptr);
	ptr->type = CREATURE_NONE;
}

void Creature_swap(CREATURE* ptr, CREATURE* other)
{
	assert(ptr);
	assert(other);
	static CREATURE buffer;
	if (ptr != other)
	{
		memcpy(&buffer, ptr, sizeof(CREATURE));
		memcpy(ptr, other, sizeof(CREATURE));
		memcpy(other,&buffer, sizeof(CREATURE));
	}
}

typedef void(*CreatureTimerFunc)(CREATURE*);

static void DoTagonTimers(CREATURE* creature)
{
	//TODO: hunger timer
}

static void DoNothingTimers(CREATURE* creature)
{
	//nothing
}

static CreatureTimerFunc creatureTimerFuncs[CREATURE_COUNT+1] =
{
	DoTagonTimers,
	DoNothingTimers
};

void Creature_doTimers(CREATURE* ptr)
{
	assert(ptr);
	creatureTimerFuncs[Creature_getType(ptr)](ptr);
}

void Creature_postInteract(CREATURE* ptr)
{
	assert(ptr);
	switch (Creature_getType(ptr))
	{
	case CREATURE_TAGON:
		Tagon_postInteract(Creature_getTagon(ptr));
		break;
	}
}

CREATURETYPE Creature_getType(CREATURE* ptr)
{
	assert(ptr);
	return ptr->type;
}

TAGON* Creature_getTagon(CREATURE* ptr)
{
	assert(ptr);
	return &(ptr->tagon);
}

CREATURECOMMON* Creature_getCommon(CREATURE* ptr)
{
	assert(ptr);
	return &(ptr->common);
}
