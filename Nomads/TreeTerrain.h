#ifndef TREE_TERRAIN_H
#define TREE_TERRAIN_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct TreeTerrain TREETERRAIN;

int TreeTerrain_getWoodLeft(TREETERRAIN* ptr);
int TreeTerrain_hasWoodLeft(TREETERRAIN* ptr);
void TreeTerrain_setWoodLeft(TREETERRAIN* ptr, int woodLeft); 
void TreeTerrain_changeWoodLeftBy(TREETERRAIN* ptr, int delta);

int TreeTerrain_getAcornTimer(TREETERRAIN* ptr);
void TreeTerrain_setAcornTimer(TREETERRAIN* ptr, int acornTimer);
void TreeTerrain_changeAcornTimerBy(TREETERRAIN* ptr, int delta);

#ifdef __cplusplus
}
#endif

#endif

