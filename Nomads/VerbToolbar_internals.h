#ifndef VERB_TOOLBAR_INTERNALS_H
#define VERB_TOOLBAR_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "VerbToolbar.h"
#include "ViewCommon_internals.h"
#include "Scroller_internals.h"

struct VerbToolbar {
	VIEWCOMMON viewCommon;
	VERBTYPE verbs[VERB_COUNT];
	SCROLLER scroller;
};

SCROLLER* VerbToolbar_getScroller(VERBTOOLBAR* ptr);

#ifdef __cplusplus
}
#endif

#endif

