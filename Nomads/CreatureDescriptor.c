#include "CreatureDescriptor_internals.h"
#include "Creature_internals.h"
#include <assert.h>

typedef void(*CreatureInstanceInitializeFunc)(CREATURE*, CREATUREDESCRIPTOR*);

static void initializeTagon(CREATURE* ptr, CREATUREDESCRIPTOR* descriptor)
{
	assert(ptr);
	assert(descriptor);
	TAGON* tagon = Creature_getTagon(ptr);
	Inventory_clear(Tagon_getInventory(tagon));
	Statistic_initialize(Tagon_getStatistic(tagon, STATISTIC_ENERGY), 100, 0, 100);
	Statistic_initialize(Tagon_getStatistic(tagon, STATISTIC_HEALTH), 100, 0, 100);
}

static void initializeNone(CREATURE* ptr, CREATUREDESCRIPTOR* descriptor)
{
	assert(ptr);
	assert(descriptor);
}

static CreatureInstanceInitializeFunc creatureInstanceInitializeFuncs[CREATURE_NONE + 1] =
{
	initializeTagon,
	initializeNone
};

void CreatureDescriptor_initializeInstance(CREATUREDESCRIPTOR* descriptor, union Creature* ptr)
{
	assert(ptr);
	assert(descriptor);
	ptr->type = descriptor->common.type.creatureType;
	creatureInstanceInitializeFuncs[descriptor->common.type.creatureType](ptr, descriptor);
}

////////////////////////////////////////////////////////////////////////////////
//Verbs
////////////////////////////////////////////////////////////////////////////////

typedef int(*CreatureDescriptorCanDoVerbFunc)(CREATUREDESCRIPTOR*, CREATURE*,VERBTYPE);

static int canDoVerbTagon(CREATUREDESCRIPTOR* descriptor, CREATURE* instance,VERBTYPE verb)
{
	assert(instance);
	assert(descriptor);
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	TAGON* tagon = Creature_getTagon(instance);
	switch(verb)
	{
	case VERB_USE_WOOD_AXE:
		return Statistic_hasAtLeast(Tagon_getStatistic(tagon, STATISTIC_ENERGY), 1);
	case VERB_PICK_BERRY:
	case VERB_LOOK:
		return 1;
	case VERB_TAKE_LOG:
	case VERB_USE_WOOD_PICK:
		return Statistic_hasAtLeast(Tagon_getStatistic(tagon, STATISTIC_ENERGY), 1);
	case VERB_USE_WOOD_SPEAR:
		return Statistic_hasAtLeast(Tagon_getStatistic(tagon, STATISTIC_ENERGY), 1);
	default:
		return 0;
	}
}

static int canDoVerbNone(CREATUREDESCRIPTOR* descriptor, CREATURE* instance,VERBTYPE verb)
{
	assert(instance);
	assert(descriptor);
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	return 0;
}

static CreatureDescriptorCanDoVerbFunc creatureDescriptorCanDoVerbFuncs[CREATURE_NONE+1]=
{
	canDoVerbTagon,
	canDoVerbNone
};

int CreatureDescriptor_canDoVerb(CREATUREDESCRIPTOR* descriptor, union Creature* ptr,VERBTYPE verb)
{
	assert(ptr);
	assert(descriptor);
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	return creatureDescriptorCanDoVerbFuncs[CreatureDescriptor_getType(descriptor)](descriptor,ptr,verb);
}

DESCRIPTORCOMMON* CreatureDescriptor_getCommon(CREATUREDESCRIPTOR* ptr)
{
	assert(ptr);
	return &(ptr->common);
}

CREATURETYPE CreatureDescriptor_getType(CREATUREDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getCreatureType(CreatureDescriptor_getCommon(ptr));
}

PATTERNTYPE CreatureDescriptor_getPattern(CREATUREDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getPattern(CreatureDescriptor_getCommon(ptr));
}

COLORTYPE CreatureDescriptor_getForeground(CREATUREDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getForeground(CreatureDescriptor_getCommon(ptr));
}

COLORTYPE CreatureDescriptor_getBackground(CREATUREDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getBackground(CreatureDescriptor_getCommon(ptr));
}


