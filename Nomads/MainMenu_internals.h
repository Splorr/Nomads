#ifndef MAIN_MENU_INTERNALS_H
#define MAIN_MENU_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "MainMenu.h"
#include "ViewCommon_internals.h"

	struct MainMenu {
		VIEWCOMMON viewCommon;
		MAINMENUITEMTYPE currentItem;
	};

#ifdef __cplusplus
}
#endif

#endif

