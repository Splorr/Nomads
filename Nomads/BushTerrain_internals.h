#ifndef BERRY_TERRAIN_INTERNALS_H
#define BERRY_TERRAIN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "BushTerrain.h"
#include "TerrainCommon_internals.h"

	struct BushTerrain {
		TERRAINCOMMON common;
		BERRYTYPE berry;
		int berryTimer;
		int deathTimer;
	};

TERRAINCOMMON* BushTerrain_getCommon(BUSHTERRAIN* ptr);

union TerrainDescriptor;

void BushTerrain_doTimer(union TerrainDescriptor*, struct MapCell*);

void DeadBushTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);//TODO: move me to deadbush.h
void BushTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);

#ifdef __cplusplus
}
#endif

#endif

