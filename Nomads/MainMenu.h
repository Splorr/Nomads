#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewCommon.h"
#include "ViewCommandType.h"

typedef enum MainMenuItemType{
	MAINMENUITEM_CONTINUE,
	MAINMENUITEM_LOAD,
	MAINMENUITEM_SAVE,
	MAINMENUITEM_QUIT,
	MAINMENUITEM_COUNT
} MAINMENUITEMTYPE;

typedef struct MainMenu MAINMENU;

union View;

void MainMenu_draw(union View*);
int MainMenu_process(union View*,VIEWCOMMANDTYPE);
void MainMenu_initialize(union View*);

#ifdef __cplusplus
}
#endif

#endif

