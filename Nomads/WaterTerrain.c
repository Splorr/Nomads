#include "WaterTerrain_internals.h"
#include <assert.h>
#include "Terrain_internals.h"
#include "Generators.h"

int WaterTerrain_getFishLeft(WATERTERRAIN* ptr)
{
	assert(ptr);
	return ptr->fishLeft;
}

int WaterTerrain_hasFishLeft(WATERTERRAIN* ptr)
{
	assert(ptr);
	return WaterTerrain_getFishLeft(ptr) > 0;
}

void WaterTerrain_setFishLeft(WATERTERRAIN* ptr, int fishLeft)
{
	assert(ptr);
	ptr->fishLeft = fishLeft;
}

void WaterTerrain_changeFishLeftBy(WATERTERRAIN* ptr, int delta)
{
	assert(ptr);
	WaterTerrain_setFishLeft(ptr, WaterTerrain_getFishLeft(ptr) + delta);
}

int WaterTerrain_getMaximumFish(WATERTERRAIN* ptr)
{
	assert(ptr);
	return ptr->maximumFish;
}

int WaterTerrain_getRestockTimer(WATERTERRAIN* ptr)
{
	assert(ptr);
	return ptr->restockTimer;
}

void WaterTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_WATER;
	ptr->water.fishLeft = Generator_generate(Generators_getGenerator(GENERATOR_FISHLEFT));
	ptr->water.maximumFish = Generator_generate(Generators_getGenerator(GENERATOR_MAXIMUMFISH));
	ptr->water.restockTimer = Generator_generate(Generators_getGenerator(GENERATOR_RESTOCKTIMER));
}

