#ifndef MAP_COLUMN_H
#define MAP_COLUMN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Constants.h"
#include "MapCell.h"

typedef struct MapColumn MAPCOLUMN;

MAPCELL* MapColumn_getMapCell(MAPCOLUMN* ptr, int row);
void MapColumn_doTimers(MAPCOLUMN* ptr);

#ifdef __cplusplus
}
#endif

#endif

