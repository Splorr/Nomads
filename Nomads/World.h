#ifndef WORLD_H
#define WORLD_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Avatar.h"
#include "Map.h"
#include "MessageFeed.h"

typedef struct World WORLD;

void World_initialize(WORLD* ptr);
void World_doTimers(WORLD* ptr);
TAGON* World_getTagon(WORLD* ptr);
INVENTORY* World_getGroundInventory(WORLD* ptr);

AVATAR* World_getAvatar(WORLD* ptr);
MAP* World_getMap(WORLD* ptr);
MESSAGEFEED* World_getMessageFeed(WORLD* ptr);
void World_writeMessage(WORLD* ptr, const char* message, COLORTYPE color);

#ifdef __cplusplus
}
#endif

#endif

