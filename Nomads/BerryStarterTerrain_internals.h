#ifndef BERRYSTARTER_TERRAIN_INTERNALS_H
#define BERRYSTARTER_TERRAIN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "BerryStarterTerrain.h"
#include "TerrainCommon_internals.h"

	struct BerryStarterTerrain {
		TERRAINCOMMON common;
		int growthTimer;
	};

	union TerrainDescriptor;
	struct MapCell;
	void BerryStarterTerrain_doTimer(union TerrainDescriptor* ptr, struct MapCell* cell);
	void BerryStarterTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);


#ifdef __cplusplus
}
#endif

#endif

