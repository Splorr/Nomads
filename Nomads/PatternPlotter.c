#include "PatternPlotter.h"
#include "Patterns.h"
#include "PixelPlotter.h"
#include <assert.h>


void PatternPlotter_plotPattern(int x, int y, PATTERNTYPE pattern, COLORTYPE foreground, COLORTYPE background)
{
	assert(pattern >= PATTERN_FIRST && pattern < PATTERN_COUNT);
	assert(foreground >= COLOR_FIRST && foreground <= COLOR_COUNT);
	assert(background >= COLOR_FIRST && background <= COLOR_COUNT);
	PATTERN* pat = Patterns_getPattern(pattern);
	COLORTYPE currentColor;
	for (int patternX = 0, plotX = x; patternX < PATTERN_WIDTH; ++patternX, ++plotX)
	{
		for (int patternY = 0, plotY = y; patternY < PATTERN_HEIGHT; ++patternY, ++plotY)
		{
			currentColor = (Pattern_getPixel(pat, patternX, patternY)) ? (foreground) : (background);
			if (currentColor < COLOR_COUNT)
			{
				PixelPlotter_plotPixel(plotX, plotY,currentColor);
			}
		}
	}
}
