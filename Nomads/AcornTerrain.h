#ifndef ACORN_TERRAIN_H
#define ACORN_TERRAIN_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct AcornTerrain ACORNTERRAIN;

	int AcornTerrain_getTreeTimer(ACORNTERRAIN* ptr);
	void AcornTerrain_setTreeTimer(ACORNTERRAIN* ptr, int treeTimer);
	void AcornTerrain_changeTreeTimerBy(ACORNTERRAIN* ptr, int delta);


#ifdef __cplusplus
}
#endif

#endif

