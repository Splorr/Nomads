#ifndef INVENTORY_INTERNALS_H
#define INVENTORY_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Inventory.h"

	struct Inventory {
		int counts[ITEM_COUNT];
	};

#ifdef __cplusplus
}
#endif

#endif

