#include "FlintrockTerrain_internals.h"
#include <assert.h>
#include "Terrain_internals.h"
#include "Generators.h"

int FlintrockTerrain_getFlintLeft(FLINTROCKTERRAIN* ptr)
{
	assert(ptr);
	return ptr->flintLeft;
}

int FlintrockTerrain_hasFlintLeft(FLINTROCKTERRAIN* ptr)
{
	assert(ptr);
	return FlintrockTerrain_getFlintLeft(ptr) > 0;
}

void FlintrockTerrain_setFlintLeft(FLINTROCKTERRAIN* ptr, int flintLeft)
{
	assert(ptr);
	ptr->flintLeft = flintLeft;
}

void FlintrockTerrain_changeFlintLeftBy(FLINTROCKTERRAIN* ptr, int delta)
{
	assert(ptr);
	FlintrockTerrain_setFlintLeft(ptr, FlintrockTerrain_getFlintLeft(ptr) + delta);
}

void FlintrockTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_FLINT_ROCK;
	ptr->flintrock.flintLeft = Generator_generate(Generators_getGenerator(GENERATOR_FLINTLEFT));
}

