#include "GravelDescriptor_internals.h"
#include <assert.h>
#include "Game.h"
#include "VerbDescriptors.h"
#include "ItemDescriptor.h"
#include "TerrainDescriptors.h"

void GravelDescriptor_use(union ItemDescriptor* itemDescriptor, struct Tagon* tagon, VERBTYPE verb)
{
	assert(itemDescriptor);
	assert(tagon);
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	ITEMTYPE itemType = ItemDescriptor_getType(itemDescriptor);
	MAPCELL* cell = Game_getTagonMapCell();
	TERRAIN* terrain = MapCell_getTerrain(cell);
	TERRAINTYPE terrainType = Terrain_getType(terrain);
	VERBDESCRIPTOR* verbDescriptor = VerbDescriptors_getDescriptor(verb);
	if (terrainType == TERRAIN_GRASS || terrainType==TERRAIN_DIRT)
	{
		if (Tagon_getCurrentStatistic(tagon, STATISTIC_ENERGY) < VerbDescriptors_getEnergy(verb))
		{
			Game_writeMessage("Not enough energy!", COLOR_SILVER);
		}
		else
		{
			Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, -1);
			Tagon_changeInventoryCountBy(tagon, itemType, -1);
			TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_RUBBLE), MapCell_getTerrain(cell));
			Game_writeMessage("Placed gravel.", COLOR_SILVER);
		}
	}
	else
	{
		Game_writeMessage("Doesn't work!", COLOR_SILVER);
	}
}

