#ifndef UTILITY_H
#define UTILITY_H

#ifdef __cplusplus
extern "C" {
#endif

int Utility_modulo(int value, int max);
int Utility_minimum(int first, int second);
int Utility_maximum(int first, int second);

#ifdef __cplusplus
}
#endif

#endif

