#include "Generator.h"
#include <stdlib.h>
#include <assert.h>
#include "Utility.h"

void Generator_initialize(GENERATOR* ptr, int count, const GENERATORENTRY* entries)
{
	assert(ptr);
	assert(count >= 1);
	assert(entries);
	ptr->count = count;
	ptr->entries = entries;
}

int Generator_generate(GENERATOR* ptr)
{
	assert(ptr);
	int total = 0;
	for (int index = 0; index < ptr->count; ++index)
	{
		total += ptr->entries[index].weight;
	}
	int generated = rand() % total;
	for (int index = 0; index < ptr->count; ++index)
	{
		if (generated < ptr->entries[index].weight)
		{
			return ptr->entries[index].value;
		}
		else
		{
			generated -= ptr->entries[index].weight;
		}
	}
	return -1;
}
