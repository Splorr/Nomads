#ifndef PATTERN_H
#define PATTERN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Constants.h"

typedef struct Pattern{
	unsigned char lines[PATTERN_HEIGHT];
} PATTERN;

int Pattern_getPixel(PATTERN* ptr, int x, int y);

#ifdef __cplusplus
}
#endif

#endif


