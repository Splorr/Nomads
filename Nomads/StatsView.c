#include "View_internals.h"
#include "TextPlotter.h"
#include "PixelPlotter_internals.h"
#include "PatternPlotter.h"
#include "Views.h"
#include "Constants.h"
#include <stdio.h>
#include "Game.h"
#include "Utility.h"
#include <assert.h>

#define STATSVIEW_OFFSET_X (0*PATTERN_WIDTH)
#define STATSVIEW_OFFSET_Y (0*PATTERN_HEIGHT)
#define STATSVIEW_WIDTH (SCALE_WIDTH(SCALE_SMALL))
#define STATSVIEW_HEIGHT (1*PATTERN_HEIGHT)

void StatsView_initialize(union View* ptr)
{
	assert(ptr);
	PixelPlotterState_initialize(View_getPixelPlotterState(ptr),
	STATSVIEW_OFFSET_X,
	STATSVIEW_OFFSET_Y,
	SCALE_SMALL,
	SCALE_SMALL,
	1,
	STATSVIEW_WIDTH,
	STATSVIEW_HEIGHT);
	ptr->viewCommon.viewType=VIEW_STATS;
	ptr->viewCommon.enabled=1;
}

void StatsView_draw(union View* ptr)
{
	assert(ptr);
	PIXELPLOTTERSTATE old;
	PixelPlotter_getState(&old);
	PixelPlotter_setState(View_getPixelPlotterState(ptr));

	PixelPlotter_plotRect(0, 0, STATSVIEW_WIDTH, STATSVIEW_HEIGHT, COLOR_DARKER_SILVER);

	PatternPlotter_plotPattern(0*PATTERN_WIDTH,0*PATTERN_HEIGHT,PATTERN_ENERGY,COLOR_TURQUOISE,COLOR_TRANSPARENT);
	PatternPlotter_plotPattern(5*PATTERN_WIDTH,0*PATTERN_HEIGHT,PATTERN_CHARACTER_03,COLOR_CARNELIAN,COLOR_TRANSPARENT);

	TAGON* tagon = Game_getTagon();
	char buffer[10];
	snprintf(buffer,10,"%3d",Statistic_getCurrent(Tagon_getStatistic(tagon,STATISTIC_ENERGY)));
	TextPlotter_plotText(1*PATTERN_WIDTH,0*PATTERN_HEIGHT,buffer,COLOR_TURQUOISE,COLOR_TRANSPARENT);
	snprintf(buffer,10,"%3d",Statistic_getCurrent(Tagon_getStatistic(tagon,STATISTIC_HEALTH)));
	TextPlotter_plotText(6*PATTERN_WIDTH,0*PATTERN_HEIGHT,buffer,COLOR_CARNELIAN,COLOR_TRANSPARENT);

	PixelPlotter_setState(&old);
}

int StatsView_process(union View* ptr, VIEWCOMMANDTYPE command)
{
	assert(ptr);
	return 0;
}


