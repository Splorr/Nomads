#ifndef MESSAGE_FEED_H
#define MESSAGE_FEED_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Constants.h"
#include "ColorType.h"
#include "ViewCommandType.h"

#define MESSAGEFEED_COLUMNS (SCALE_WIDTH(SCALE_SMALL)/PATTERN_WIDTH)
#define MESSAGEFEED_ROWS (3)

typedef struct MessageFeedLine{
	char text[MESSAGEFEED_COLUMNS+1];
	COLORTYPE color;
} MESSAGEFEEDLINE;

typedef struct MessageFeed{
	MESSAGEFEEDLINE lines[MESSAGEFEED_ROWS];
} MESSAGEFEED;

union View;

void MessageFeed_writeMessage(MESSAGEFEED* ptr, const char* message, COLORTYPE color);
void MessageFeed_draw(union View*);
int MessageFeed_process(union View*,VIEWCOMMANDTYPE);
void MessageFeed_initialize(union View*);



#ifdef __cplusplus
}
#endif

#endif

