#ifndef QUICK_HELP_H
#define QUICK_HELP_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewCommandType.h"

union View;

void QuickHelp_draw(union View*);
int QuickHelp_process(union View*, VIEWCOMMANDTYPE);
void QuickHelp_initialize(union View*);

#ifdef __cplusplus
}
#endif

#endif





