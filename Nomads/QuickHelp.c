#include "View_internals.h"
#include "TextPlotter.h"
#include "PixelPlotter_internals.h"
#include "PatternPlotter.h"
#include "Views.h"
#include "Constants.h"
#include <stdio.h>
#include "Game.h"
#include "Utility.h"
#include <assert.h>

#define QUICKHELP_OFFSET_X (0)
#define QUICKHELP_OFFSET_Y (0)
#define QUICKHELP_WIDTH (SCALE_WIDTH(SCALE_SMALL))
#define QUICKHELP_HEIGHT (SCALE_HEIGHT(SCALE_SMALL))
#define QUICKHELP_ROWS ((QUICKHELP_HEIGHT)/(PATTERN_HEIGHT))
#define QUICKHELP_COLUMNS  ((QUICKHELP_WIDTH)/(PATTERN_WIDTH))

void QuickHelp_initialize(union View* ptr)
{
	assert(ptr);
	PixelPlotterState_initialize(View_getPixelPlotterState(ptr),
		QUICKHELP_OFFSET_X,
		QUICKHELP_OFFSET_Y,
		SCALE_SMALL,
		SCALE_SMALL,
		1,
		QUICKHELP_WIDTH,
		QUICKHELP_HEIGHT);
	ptr->viewCommon.viewType = VIEW_QUICK_HELP;
	ptr->viewCommon.enabled = 1;
}

void QuickHelp_draw(union View* ptr)
{
	assert(ptr);
	PIXELPLOTTERSTATE old;
	PixelPlotter_getState(&old);
	PixelPlotter_setState(View_getPixelPlotterState(ptr));

	PixelPlotter_plotRect(0, 0, QUICKHELP_WIDTH, QUICKHELP_HEIGHT,COLOR_DARK_TURQUOISE);

	TextPlotter_plotText(0 * PATTERN_WIDTH, 0 * PATTERN_HEIGHT, "Nomads of Splorr!!", COLOR_SILVER, COLOR_TRANSPARENT);

	TextPlotter_plotText(0 * PATTERN_WIDTH, 2 * PATTERN_HEIGHT, "In-Game Controls:", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(0 * PATTERN_WIDTH, 3 * PATTERN_HEIGHT, "WASD - Move", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(0 * PATTERN_WIDTH, 4 * PATTERN_HEIGHT, "E - Use item in hand", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(0 * PATTERN_WIDTH, 5 * PATTERN_HEIGHT, "X - Interact with \"thing\" under cursor", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(0 * PATTERN_WIDTH, 6 * PATTERN_HEIGHT, "Q - Inventory screen", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(0 * PATTERN_WIDTH, 7 * PATTERN_HEIGHT, "C - Crafting screen", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(0 * PATTERN_WIDTH, 8 * PATTERN_HEIGHT, ",. - Change item in hand", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(0 * PATTERN_WIDTH, 9 * PATTERN_HEIGHT, "Escape - Main menu", COLOR_SILVER, COLOR_TRANSPARENT);

	TextPlotter_centerText(QUICKHELP_WIDTH / 2, QUICKHELP_HEIGHT - PATTERN_HEIGHT, "Press E", COLOR_SILVER, COLOR_TRANSPARENT);

	PixelPlotter_setState(&old);
}

int QuickHelp_process(union View* ptr, VIEWCOMMANDTYPE command)
{
	assert(ptr);
	switch (command)
	{
	case VIEWCOMMAND_BUTTON_A:
		Views_disable(View_getType(ptr));
		return 1;
	default:
		return 1;
	}
}


