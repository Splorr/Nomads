#ifndef TAGON_INTERNALS_H
#define TAGON_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Tagon.h"
#include "CreatureCommon_internals.h"
#include "Statistic_internals.h"
#include "Inventory_internals.h"

struct Tagon {
	CREATURECOMMON common;
	INVENTORY inventory;
	STATISTIC statistics[STATISTIC_COUNT];
	EXERTIONTYPE exertion;
	VERBTYPE currentVerb;
};

#ifdef __cplusplus
}
#endif

#endif

