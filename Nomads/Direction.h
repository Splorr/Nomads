#ifndef DIRECTION_H
#define DIRECTION_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DirectionType.h"

int Direction_getNextX(int x,int y,DIRECTIONTYPE direction);
int Direction_getNextY(int x,int y,DIRECTIONTYPE direction);

#ifdef __cplusplus
}
#endif

#endif

