#ifndef STATS_VIEW_H
#define STATS_VIEW_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewCommandType.h"

union View;

void StatsView_draw(union View*);
int StatsView_process(union View*, VIEWCOMMANDTYPE);
void StatsView_initialize(union View*);

#ifdef __cplusplus
}
#endif

#endif





