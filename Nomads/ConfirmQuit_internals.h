#ifndef CONFIRM_QUIT_INTERNALS_H
#define CONFIRM_QUIT_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ConfirmQuit.h"
#include "ViewCommon_internals.h"

	typedef struct ConfirmQuit {
		VIEWCOMMON viewCommon;
		CONFIRMQUITITEMTYPE currentItem;
	} CONFIRMQUIT;

#ifdef __cplusplus
}
#endif

#endif
