#ifndef GENERATORS_H
#define GENERATORS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "GeneratorType.h"
#include "Generator.h"

GENERATOR* Generators_getGenerator(GENERATORTYPE generatorType);

#ifdef __cplusplus
}
#endif

#endif

