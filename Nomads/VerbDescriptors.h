#ifndef VERB_DESCRIPTORS_H
#define VERB_DESCRIPTORS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "VerbDescriptor.h"

void VerbDescriptors_initialize();
VERBDESCRIPTOR* VerbDescriptors_getDescriptor(VERBTYPE verb);
int VerbDescriptors_getEnergy(VERBTYPE verb);
ITEMTYPE VerbDescriptors_getItem(VERBTYPE verb);

#ifdef __cplusplus
}
#endif

#endif

