#ifndef COLORS_H
#define COLORS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ColorType.h"
#include "Color.h"

COLOR* Colors_getColor(COLORTYPE colorType);

#ifdef __cplusplus
}
#endif

#endif
