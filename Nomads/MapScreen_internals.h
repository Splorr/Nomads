#ifndef MAP_SCREEN_INTERNALS_H
#define MAP_SCREEN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "MapScreen.h"
#include "ViewCommon_internals.h"

	struct MapScreen{
		VIEWCOMMON viewCommon;
	};

#ifdef __cplusplus
}
#endif

#endif

