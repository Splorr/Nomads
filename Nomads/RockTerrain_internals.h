#ifndef ROCK_TERRAIN_INTERNALS_H
#define ROCK_TERRAIN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "RockTerrain.h"
#include "TerrainCommon_internals.h"

	struct RockTerrain {
		TERRAINCOMMON common;
		int stoneLeft;
	};

	void RockTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);

#ifdef __cplusplus
}
#endif

#endif

