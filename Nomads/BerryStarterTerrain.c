#include "BerryStarterTerrain_internals.h"
#include <assert.h>
#include "Inventory.h"
#include "CreatureType.h"
#include "MapCell.h"
#include "TerrainDescriptors.h"
#include "Terrain_internals.h"
#include "Generators.h"

int BerryStarterTerrain_getGrowthTimer(BERRYSTARTERTERRAIN* ptr)
{
	assert(ptr);
	return ptr->growthTimer;
}

void BerryStarterTerrain_setGrowthTimer(BERRYSTARTERTERRAIN* ptr, int growthTimer)
{
	assert(ptr);
	ptr->growthTimer = growthTimer;
}

void BerryStarterTerrain_changeGrowthTimerBy(BERRYSTARTERTERRAIN* ptr, int delta)
{
	assert(ptr);
	BerryStarterTerrain_setGrowthTimer(ptr, BerryStarterTerrain_getGrowthTimer(ptr) + delta);
}

void BerryStarterTerrain_doTimer(union TerrainDescriptor* ptr, struct MapCell* cell)
{
	assert(ptr);
	assert(cell);

	int growFlag = 1;
	INVENTORY* inventory = MapCell_getInventory(cell);
	if (Inventory_getTotalCount(inventory) > 0 || Creature_getType(MapCell_getCreature(cell)) != CREATURE_NONE)
	{
		growFlag = 0;
	}
	if (growFlag)
	{
		BERRYSTARTERTERRAIN* berry = Terrain_getBerryStarter(MapCell_getTerrain(cell));
		BerryStarterTerrain_changeGrowthTimerBy(berry, -1);
		if (BerryStarterTerrain_getGrowthTimer(berry) <= 0)
		{
			TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_BUSH), MapCell_getTerrain(cell));
		}
	}
}

void BerryStarterTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_BERRY_STARTER;
	ptr->berryStarter.growthTimer = Generator_generate(Generators_getGenerator(GENERATOR_BERRYSTARTERGROWTHTIMER));
}

