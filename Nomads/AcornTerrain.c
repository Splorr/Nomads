#include "AcornTerrain_internals.h"
#include "Utility.h"
#include <assert.h>
#include "Terrain_internals.h"
#include "Generators.h"
#include "MapCell.h"
#include "TerrainDescriptors.h"

int AcornTerrain_getTreeTimer(ACORNTERRAIN* ptr)
{
	assert(ptr);
	return ptr->treeTimer;
}

void AcornTerrain_setTreeTimer(ACORNTERRAIN* ptr, int treeTimer)
{
	assert(ptr);
	ptr->treeTimer = treeTimer;
}

void AcornTerrain_changeTreeTimerBy(ACORNTERRAIN* ptr, int delta)
{
	assert(ptr);
	AcornTerrain_setTreeTimer(ptr, AcornTerrain_getTreeTimer(ptr) + delta);
}

void AcornTerrain_doTimer(union TerrainDescriptor* ptr, struct MapCell* cell)
{
	ACORNTERRAIN* acorn = Terrain_getAcorn(MapCell_getTerrain(cell));

	AcornTerrain_changeTreeTimerBy(acorn, -1);
	if (AcornTerrain_getTreeTimer(acorn) <= 0)
	{
		TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_TREE), MapCell_getTerrain(cell));
	}
}

void AcornTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_ACORN;
	ptr->acorn.treeTimer = Generator_generate(Generators_getGenerator(GENERATOR_ACORNTOTREE));
}

