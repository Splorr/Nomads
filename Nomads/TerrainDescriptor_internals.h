#ifndef TERRAIN_DESCRIPTOR_INTERNALS_H
#define TERRAIN_DESCRIPTOR_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TerrainDescriptor.h"
#include "DescriptorCommon_internals.h"

	struct MapCell;
	union Terrain;

	typedef void(*TerrainDescriptorDoTimersFunc)(TERRAINDESCRIPTOR*, struct MapCell*);
	typedef void(*TerrainInstanceInitializeFunc)(union Terrain*, TERRAINDESCRIPTOR*);

	struct TerrainDescriptorCommon {
		DESCRIPTORCOMMON common;
		TerrainDescriptorDoTimersFunc timerFunc;
		TerrainInstanceInitializeFunc initializeFunc;
		const char* name;
	};

	union TerrainDescriptor {
		struct TerrainDescriptorCommon common;
	};

	DESCRIPTORCOMMON* TerrainDescriptor_getCommon(TERRAINDESCRIPTOR* ptr);
	void TerrainDescriptor_doNothingTimer(TERRAINDESCRIPTOR*, struct MapCell*);

#ifdef __cplusplus
}
#endif

#endif

