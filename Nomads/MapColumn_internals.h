#ifndef MAP_COLUMN_INTERNALS_H
#define MAP_COLUMN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Constants.h"
#include "MapCell_internals.h"
#include "MapColumn.h"

struct MapColumn {
	MAPCELL cells[MAP_ROWS];
};

#ifdef __cplusplus
}
#endif

#endif

