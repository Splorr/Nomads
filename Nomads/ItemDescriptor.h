#ifndef ITEM_DESCRIPTOR_H
#define ITEM_DESCRIPTOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ItemType.h"
#include "BerryDescriptor.h"
#include "Tagon.h"

typedef union ItemDescriptor ITEMDESCRIPTOR;

ITEMTYPE ItemDescriptor_getType(ITEMDESCRIPTOR* ptr);
const char* ItemDescriptor_getName(ITEMDESCRIPTOR* ptr);
PATTERNTYPE ItemDescriptor_getPattern(ITEMDESCRIPTOR* ptr);
COLORTYPE ItemDescriptor_getForeground(ITEMDESCRIPTOR* ptr);
COLORTYPE ItemDescriptor_getBackground(ITEMDESCRIPTOR* ptr);
BERRYDESCRIPTOR* ItemDescriptor_getBerry(ITEMDESCRIPTOR* ptr);

#ifdef __cplusplus
}
#endif

#endif

