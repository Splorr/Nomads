#include "Direction.h"
#include <assert.h>

int Direction_getNextX(int x, int y, DIRECTIONTYPE direction)
{
	assert(direction >= DIRECTION_FIRST && direction <=DIRECTION_COUNT);
	return (((direction) == DIRECTION_EAST) ? ((x)+1) : ((direction) == DIRECTION_WEST) ? ((x)-1) : (x));
}

int Direction_getNextY(int x, int y, DIRECTIONTYPE direction)
{
	assert(direction >= DIRECTION_FIRST && direction <=DIRECTION_COUNT);
	return (((direction) == DIRECTION_SOUTH) ? ((y)+1) : ((direction) == DIRECTION_NORTH) ? ((y)-1) : (y));
}
