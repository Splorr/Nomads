#include "Avatar_internals.h"
#include "Direction.h"
#include <assert.h>

void Avatar_initialize(AVATAR* ptr, int x, int y,DIRECTIONTYPE facing)
{
	assert(ptr);
	assert(facing >= DIRECTION_NORTH && facing <= DIRECTION_COUNT);
	ptr->x = x;
	ptr->y = y;
	ptr->facing = facing;
}

void Avatar_move(AVATAR* ptr, DIRECTIONTYPE direction)
{
	assert(ptr);
	assert(direction >= DIRECTION_NORTH && direction < DIRECTION_COUNT);
	Avatar_setFacing(ptr,direction);
	int nextX = Direction_getNextX(Avatar_getX(ptr), Avatar_getY(ptr), direction);
	int nextY = Direction_getNextY(Avatar_getX(ptr), Avatar_getY(ptr), direction);
	ptr->x = nextX;
	ptr->y = nextY;
}

void Avatar_setFacing(AVATAR* ptr, DIRECTIONTYPE direction)
{
	assert(ptr);
	assert(direction >= DIRECTION_NORTH && direction <= DIRECTION_COUNT);
	ptr->facing = direction;
}

void Avatar_doTimers(AVATAR* ptr)
{
	assert(ptr);
}

int Avatar_getX(AVATAR* ptr)
{
	assert(ptr);
	return ptr->x;
}

int Avatar_getY(AVATAR* ptr)
{
	assert(ptr);
	return ptr->y;
}

DIRECTIONTYPE Avatar_getFacing(AVATAR* ptr)
{
	assert(ptr);
	return ptr->facing;
}
