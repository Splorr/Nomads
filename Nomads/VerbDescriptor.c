#include "VerbDescriptor_internals.h"
#include <assert.h>

VERBTYPE VerbDescriptor_getType(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getVerbType(&(ptr->common));
}

PATTERNTYPE VerbDescriptor_getPattern(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getPattern(&(ptr->common));
}

COLORTYPE VerbDescriptor_getForeground(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getForeground(&(ptr->common));
}

COLORTYPE VerbDescriptor_getBackground(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getBackground(&(ptr->common));
}

ITEMTYPE VerbDescriptor_getItem(VERBDESCRIPTOR* ptr)
{
	return ptr->item;
}

const char* VerbDescriptor_getCaption(VERBDESCRIPTOR* ptr)
{
	return ptr->caption;
}

int VerbDescriptor_canDoVerb(VERBDESCRIPTOR* ptr, INVENTORY* inventory)
{
	return (VerbDescriptor_getItem(ptr) == ITEM_NONE) || (Inventory_hasCount(inventory, VerbDescriptor_getItem(ptr)));
}

int VerbDescriptor_getEnergy(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	return ptr->energy;
}

void VerbDescriptor_use(VERBDESCRIPTOR* ptr, TAGON* tagon)
{
	assert(ptr);
	ptr->useFunc(ptr, tagon);
}