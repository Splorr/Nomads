#ifndef CREATURE_INTERNALS_H
#define CREATURE_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Creature.h"
#include "CreatureCommon_internals.h"
#include "Tagon_internals.h"

union Creature {
	CREATURETYPE type;
	CREATURECOMMON common;
	TAGON tagon;
};

CREATURECOMMON* Creature_getCommon(CREATURE* ptr);

#ifdef __cplusplus
}
#endif

#endif
